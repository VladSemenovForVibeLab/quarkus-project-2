package ru.semenov.periodicTasks.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;
import ru.semenov.periodicTasks.entity.Job;

@ApplicationScoped
public class JobRepository implements PanacheRepository<Job> {
}
