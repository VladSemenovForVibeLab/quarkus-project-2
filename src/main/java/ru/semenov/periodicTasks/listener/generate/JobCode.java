package ru.semenov.periodicTasks.listener.generate;

public class JobCode {
    public static String generateJobCode(String title) {
        // Логика для генерации уникального кода для работы
        // Например, можно использовать заглавные буквы из заголовка работы
        StringBuilder codeBuilder = new StringBuilder();

        for (char c : title.toCharArray()) {
            if (Character.isUpperCase(c)) {
                codeBuilder.append(c);
            }

            if (codeBuilder.length() >= 4) {
                break;
            }
        }

        return codeBuilder.toString();
    }
}
