package ru.semenov.periodicTasks.listener.history;

import ru.semenov.periodicTasks.entity.Job;

import java.io.FileWriter;
import java.io.IOException;

public class History {
    public static void saveChangeHistory(Job job) {
        // Реализация сохранения истории изменений в файле
        try (FileWriter writer = new FileWriter("change_history.txt", true)) {
            String changeDescription = "Job with ID " + job.getId() + " was updated";
            writer.write(changeDescription);
            writer.write(System.lineSeparator());
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
