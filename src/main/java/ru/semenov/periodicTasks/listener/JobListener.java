package ru.semenov.periodicTasks.listener;

import jakarta.persistence.*;
import ru.semenov.periodicTasks.entity.Job;

import static ru.semenov.periodicTasks.listener.generate.JobCode.generateJobCode;
import static ru.semenov.periodicTasks.listener.history.History.saveChangeHistory;

public class JobListener {
    @PrePersist
    public void beforePersist(Job job) {
        // Дополнительная логика перед сохранением
        System.out.println("Before persisting job: " + job.getId());
        String code = generateJobCode(job.getTitle());
        job.setCode(code);
    }

    @PostPersist
    public void afterPersist(Job job) {
        System.out.println("After persisting job: " + job.getId());
        // Дополнительная логика после сохранения
    }

    @PreUpdate
    public void beforeUpdate(Job job) {
        System.out.println("Before updating job: " + job.getId());
        // Дополнительная логика перед обновлением
        saveChangeHistory(job);
    }

    @PostUpdate
    public void afterUpdate(Job job) {
        System.out.println("After updating job: " + job.getId());
        // Дополнительная логика после обновления
    }

    @PreRemove
    public void beforeRemove(Job job) {
         System.out.println("Before removing job: " + job.getId());
        // Дополнительная логика перед удалением
    }

    @PostRemove
    public void afterRemove(Job job) {
        System.out.println("After removing job: " + job.getId());
        // Дополнительная логика после удаления
    }
}
