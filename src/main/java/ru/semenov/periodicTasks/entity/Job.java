package ru.semenov.periodicTasks.entity;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UuidGenerator;
import ru.semenov.periodicTasks.listener.JobListener;

import java.io.Serializable;

@Entity(name = "Job")
@Table(name = Job.TABLE_NAME,
        uniqueConstraints = {
                @UniqueConstraint(name = "UNIQUE_JOB_TITLE_AND_ID",
                        columnNames = {
                                "title",
                                "id"})
        },
        indexes = {
                @Index(name = "IND_JOB",
                        columnList = "id")
        })
@Getter(lazy = false,
        value = AccessLevel.PUBLIC)
@Setter(value = AccessLevel.PUBLIC)
@NoArgsConstructor(access = AccessLevel.PRIVATE,
        staticName = "create", force = true)
@AllArgsConstructor(access = AccessLevel.PUBLIC,
        staticName = "create")
@ToString(callSuper = true,
        includeFieldNames = true,
        doNotUseGetters = false,
        onlyExplicitlyIncluded = true)
@EqualsAndHashCode(
        cacheStrategy = EqualsAndHashCode.CacheStrategy.LAZY,
        callSuper = false,
        doNotUseGetters = false,
        onlyExplicitlyIncluded = true)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(
        name = "type",
        discriminatorType = DiscriminatorType.INTEGER)
@DiscriminatorValue(value = "1")
@NamedQueries({
        @NamedQuery(name = "Job.getDescriptionById", query = "SELECT j.description FROM Job j WHERE j.id = :id"),
        @NamedQuery(name = "Job.getTitleById", query = "SELECT j.title FROM Job j WHERE j.id = :id"),
        @NamedQuery(name = "Job.getIdByTitle", query = "SELECT j.id FROM Job j WHERE j.title = :title")
})
@NamedNativeQueries({
        @NamedNativeQuery(name = "Job.getDescriptionByIdNative", query = "SELECT description FROM job WHERE id = :id", resultClass = Job.class),
        @NamedNativeQuery(name = "Job.getTitleByIdNative", query = "SELECT title FROM job WHERE id = :id", resultClass = Job.class),
        @NamedNativeQuery(name = "Job.getIdByTitleNative", query = "SELECT id FROM job WHERE title = :title", resultClass = Job.class)
})
@NamedEntityGraph(name = "jobGraph",
        attributeNodes = {
                @NamedAttributeNode("description"),
                @NamedAttributeNode("title"),
                @NamedAttributeNode("id")
        })
@EntityListeners({JobListener.class})
public class Job implements Serializable, Comparable<Job>, Cloneable {
    public static final String TABLE_NAME = "job";
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_TITLE = "title";
    private static final String COLUMN_DESCRIPTION = "description";
    private static final String COLUMN_CODE ="code";
    @ToString.Include
    @EqualsAndHashCode.Include
    @Id
    @UuidGenerator
    @GeneratedValue(strategy = GenerationType.UUID, generator = "system-uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Setter(AccessLevel.NONE)
    @Column(name = COLUMN_ID, nullable = false, unique = true, insertable = false, updatable = false)
    private String id;
    @ToString.Include
    @EqualsAndHashCode.Include
    @Basic(optional = false, fetch = FetchType.EAGER)
    @Column(name = COLUMN_TITLE,
            nullable = false,
            unique = true,
            insertable = true,
            updatable = true,
            columnDefinition = "VARCHAR(255)",
            length = 50)
    private String title;
    @ToString.Include
    @Lob
    @Basic(optional = true, fetch = FetchType.EAGER)
    @Column(name = COLUMN_DESCRIPTION,
            nullable = true,
            unique = false,
            insertable = true,
            updatable = true,
            columnDefinition = "TEXT")
    private String description;
    @Column(name = COLUMN_CODE,nullable = true,unique = false)
    @Basic(optional = true,fetch = FetchType.LAZY)
    private String code;

    @Override
    public int compareTo(Job o) {
        return this.title.compareTo(o.title);
    }

    @Override
    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            throw new InternalError(e.toString());
        }
    }
}
