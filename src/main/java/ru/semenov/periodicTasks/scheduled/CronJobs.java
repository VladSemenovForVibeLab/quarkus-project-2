package ru.semenov.periodicTasks.scheduled;

import io.quarkus.scheduler.Scheduled;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class CronJobs {
    @Scheduled(every = "10s")
    public void executeEvery10s(){
        System.out.println("JOB!");
    }
    @Scheduled(cron = "0 53 12 * * ?")
    public void executeEveryDay(){
        System.out.println("Executing at 12");
    }
}
