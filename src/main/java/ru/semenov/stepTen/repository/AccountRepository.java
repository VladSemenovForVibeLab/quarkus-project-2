package ru.semenov.stepTen.repository;

import io.quarkus.arc.Lock;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.persistence.LockModeType;
import ru.semenov.stepTen.entity.Account;

@ApplicationScoped
public class AccountRepository implements PanacheRepository<Account> {
    public Account findByIdString(String accountId) {
        return find("id",accountId).firstResult();
    }

    public Account findByNumber(Long number) {
        return find("number",number).firstResult();
    }

    @Override
    public void persist(Account account) {
        PanacheRepository.super.persist(account);
    }
}
