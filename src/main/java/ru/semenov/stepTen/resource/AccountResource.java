package ru.semenov.stepTen.resource;

import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.jboss.logging.Logger;
import ru.semenov.stepTen.dto.AccountDTO;
import ru.semenov.stepTen.dto.AmountDTO;
import ru.semenov.stepTen.service.AccountService;

@Path(AccountResource.ACCOUNT_RESOURCE_URI)
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AccountResource {

    public static final Logger LOG = Logger.getLogger(AccountResource.class);
    public static final String ACCOUNT_RESOURCE_URI = "/api/v1/accounts";

    @Inject
    private AccountService accountService;

    @POST
    @Path("/open_account")
    public Response openAccount(@RequestBody AccountDTO accountDTO){
        LOG.info("Entering in Response::openAccount()");
        boolean alreadyExist = accountService.isAccountAlreadyExist(accountDTO);
        if (alreadyExist) {
            return Response.ok("Ooops! Account already exists!").build();
        }else {
            return Response.ok("Thanks for opening the account").build();
        }
    }

    @POST
    @Path("/{number}/addAmount")
    public Response deposit(@RequestBody AmountDTO amountDTO,
                            @PathParam("number") Long number){
        synchronized (this) {
            LOG.info("DEPOSIT ACCOUNT");
            boolean deposit = accountService.deposit(number, amountDTO);
            if (deposit) {
                return Response.ok("Your account is funded by " + amountDTO.amount()).build();
            } else {
                return Response.status(Response.Status.BAD_REQUEST).build();
            }
        }
    }
}
