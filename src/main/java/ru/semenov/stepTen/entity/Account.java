package ru.semenov.stepTen.entity;

import jakarta.persistence.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UuidGenerator;

import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = Account.TABLE_NAME)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type",discriminatorType = DiscriminatorType.INTEGER)
public class Account implements Serializable,Cloneable,Comparable<Account> {
    public static final String TABLE_NAME = "account";

    private static final String COLUMN_ID = "id";
    private static final String COLUMN_ACCOUNT_NUMBER = "account_number";

    private static final String COLUMN_NAME="name";
    private static final String COLUMN_NUMBER = "number";
    private static final String COLUMN_AMOUNT="amount";
    @Id
    @UuidGenerator
    @GeneratedValue(strategy = GenerationType.UUID,generator = "system-uuid")
    @GenericGenerator(name = "uuid",strategy = "uuid2")
    @Column(name = COLUMN_ID,nullable = false,unique = true)
    private String id;
    @Basic(optional = false,fetch = FetchType.EAGER)
    @Column(name = COLUMN_ACCOUNT_NUMBER,nullable = false,unique = true)
    private Long accountNumber;

    @Basic(optional = false,fetch = FetchType.EAGER)
    @Column(name = COLUMN_NAME,nullable = false)
    private String name;

    @Basic(optional = false,fetch = FetchType.EAGER)
    @Column(name = COLUMN_NUMBER,nullable = false,unique = true)
    private Long number;

    @Basic(optional = true,fetch = FetchType.EAGER)
    @Column(name = COLUMN_AMOUNT)
    private Integer amount = 0;
    @Transient
    private final Object lock = new Object();

    public void addToAmount(Integer amount){
        synchronized (lock) {
            this.amount+=amount;
        }
    }

    public Account(String name, Long number) {
        this.name = name;
        this.number = number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return Objects.equals(id, account.id) && Objects.equals(accountNumber, account.accountNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, accountNumber);
    }

    @Override
    public String toString() {
        return "Account{" +
                "id='" + id + '\'' +
                ", accountNumber=" + accountNumber +
                ", name='" + name + '\'' +
                ", number=" + number +
                ", amount=" + amount +
                ", lock=" + lock +
                '}';
    }

    public Account(Long accountNumber, String name, Long number, Integer amount) {
        this.accountNumber = accountNumber;
        this.name = name;
        this.number = number;
        this.amount = amount;
    }

    public Account() {
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Object getLock() {
        return lock;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(Long accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int compareTo(Account o) {
        return this.name.compareTo(o.getName());
    }
    @Override
    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            throw new InternalError(e.toString());
        }
    }

}
