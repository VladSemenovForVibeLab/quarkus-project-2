package ru.semenov.stepTen.service;

import ru.semenov.stepTen.dto.AccountDTO;
import ru.semenov.stepTen.dto.AmountDTO;

public interface AccountService {
    boolean isAccountAlreadyExist(AccountDTO accountDTO);

    boolean deposit(Long number, AmountDTO amountDTO);
}
