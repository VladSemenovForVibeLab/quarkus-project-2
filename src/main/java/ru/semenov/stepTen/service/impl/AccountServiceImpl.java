package ru.semenov.stepTen.service.impl;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import org.jboss.logging.Logger;
import ru.semenov.stepTen.dto.AccountDTO;
import ru.semenov.stepTen.dto.AmountDTO;
import ru.semenov.stepTen.entity.Account;
import ru.semenov.stepTen.repository.AccountRepository;
import ru.semenov.stepTen.service.AccountService;

import java.util.Random;

@ApplicationScoped
public class AccountServiceImpl implements AccountService {

    public static final Logger LOG = Logger.getLogger(AccountService.class);

    @Inject
    private AccountRepository accountRepository;

    @Override
    @Transactional
    public boolean isAccountAlreadyExist(AccountDTO accountDTO) {
        Account account = getAccountByNumber(accountDTO.getNumber());
        if(account!=null){
            LOG.debug("Account already exists");
            return true;
        }else{
            LOG.debug("Account not found");
            return openNewAccount(accountDTO);
        }
    }

    @Override
    public boolean deposit(Long number, AmountDTO amountDTO) {
        LOG.info("DEPOSIT SERVICE");
        Account account = accountRepository.findByNumber(number);
        if (account!=null){
            synchronized (account){
                account.addToAmount(amountDTO.amount());
                accountRepository.persist(account);
            }
            return true;
        }else {
            return false;
        }
    }

    private boolean openNewAccount(AccountDTO accountDTO) {
        LOG.debug("Open new account");
        Account account = new Account(
                accountDTO.getName(),
                accountDTO.getNumber()
        );
        account.setAccountNumber(new Random().nextLong());
        accountRepository.persist(account);
        return false;
    }

    private Account getAccountByNumber(Long number) {
        LOG.debug("Get account");
        return accountRepository.find("number",number).firstResult();
    }
}
