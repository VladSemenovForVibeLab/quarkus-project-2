package ru.semenov.stepTen.dto;

public record AmountDTO(Integer amount) {
}
