package ru.semenov.stepTen.dto;

public record AccountDTORecord(
        String name,
        Long number,
        Integer amount) {
}
