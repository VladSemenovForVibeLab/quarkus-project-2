package ru.semenov.collection.stack;

public interface IStack<T> {
    boolean isEmpty();
    int size();
    void push(T value);
    T pop();
    T peek();
}
