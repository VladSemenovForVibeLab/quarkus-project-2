package ru.semenov.collection.stack.impl;

import lombok.Getter;
import ru.semenov.collection.stack.IStack;
@Getter
public class IStackImpl<T> implements IStack<T> {
    private Node<T> head;
    private int size;

    public IStackImpl(){
        head = null;
        size = 0;
    }
    @Override
    public boolean isEmpty() {
        return size==0;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void push(T value) {
        Node<T> newNode = new Node<>(value);
        if(isEmpty()){
            head = newNode;
        }else {
            newNode.next=head;
            head=newNode;
        }
        size++;
    }

    @Override
    public T pop() {
        if(isEmpty()){
            throw new IllegalStateException("Stack is empty");
        }
        T data = head.data;
        head = head.next;
        size--;
        return data;
    }

    @Override
    public T peek() {
      if(isEmpty()){
          throw new IllegalStateException("Stack is empty");
      }
      return  head.data;
    }
    @Getter
    private static class Node<T>{
        private T data;
        private Node<T> next;

        public Node(T data) {
            this.data = data;
            this.next = null;
        }
    }
}
