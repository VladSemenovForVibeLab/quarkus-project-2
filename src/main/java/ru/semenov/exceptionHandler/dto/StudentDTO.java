package ru.semenov.exceptionHandler.dto;

public record StudentDTO(String name,
                         String branch) {
}
