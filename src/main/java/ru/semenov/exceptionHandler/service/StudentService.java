package ru.semenov.exceptionHandler.service;

import ru.semenov.exceptionHandler.dto.StudentDTO;
import ru.semenov.exceptionHandler.exception.BusinessException;
import ru.semenov.stepSevenTesting.entity.Student;

public interface StudentService {
    Student getStudent(String studentId) throws BusinessException;

    Student createStudent(StudentDTO studentDTO);
}
