package ru.semenov.exceptionHandler.service.impl;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.core.Response;
import ru.semenov.exceptionHandler.dto.StudentDTO;
import ru.semenov.exceptionHandler.exception.BusinessException;
import ru.semenov.exceptionHandler.mapper.StudentMapperDTO;
import ru.semenov.exceptionHandler.service.StudentService;
import ru.semenov.stepSevenTesting.entity.Student;
import ru.semenov.stepSevenTesting.repository.StudentRepository;

@ApplicationScoped
public class StudentServiceImpl implements StudentService {
    @Inject
    private StudentRepository studentRepository;

    @Inject
    private StudentMapperDTO studentMapperDTO;

    @Override
    public Student getStudent(String studentId) throws BusinessException {
        Student student = studentRepository.findByIdString(studentId);
        if(student == null){
            throw new  BusinessException(
                    Response.Status.NOT_FOUND.getStatusCode()
                    ,"Пользователь не найден "+studentId);
        }else {
            return student;
        }
    }

    @Override
    @Transactional
    public Student createStudent(StudentDTO studentDTO) {
        Student entity = studentMapperDTO.toEntity(studentDTO);
        studentRepository.persist(entity);
        return entity;
    }
}

