package ru.semenov.exceptionHandler.mapper;

public interface EntityDTOMapper <E,D>{
    E toEntity (D dto);
}
