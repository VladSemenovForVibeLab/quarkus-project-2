package ru.semenov.exceptionHandler.mapper;

import jakarta.enterprise.context.ApplicationScoped;
import ru.semenov.exceptionHandler.dto.StudentDTO;
import ru.semenov.stepSevenTesting.entity.Student;

@ApplicationScoped
public class StudentMapperDTO implements EntityDTOMapper<Student, StudentDTO> {
    @Override
    public Student toEntity(StudentDTO dto) {
        Student student = new Student();
        student.setName(dto.name());
        student.setBranch(dto.branch());
        return student;
    }
}
