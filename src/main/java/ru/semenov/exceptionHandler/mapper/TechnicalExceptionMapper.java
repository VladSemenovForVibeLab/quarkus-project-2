package ru.semenov.exceptionHandler.mapper;

import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;
import ru.semenov.exceptionHandler.dto.ErrorMessage;
import ru.semenov.exceptionHandler.exception.TechnicalException;

@Provider
public class TechnicalExceptionMapper implements ExceptionMapper<TechnicalException> {
    @Override
    public Response toResponse(TechnicalException e) {
        ErrorMessage errorMessage = new ErrorMessage();
        errorMessage.setStatus(e.getStatus());
        errorMessage.setMessage(e.getMessage());
        return Response
                .status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity(errorMessage)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }
}
