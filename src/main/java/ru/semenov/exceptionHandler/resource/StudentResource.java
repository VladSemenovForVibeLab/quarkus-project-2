package ru.semenov.exceptionHandler.resource;

import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import ru.semenov.exceptionHandler.dto.StudentDTO;
import ru.semenov.exceptionHandler.exception.BusinessException;
import ru.semenov.exceptionHandler.exception.TechnicalException;
import ru.semenov.exceptionHandler.service.StudentService;
import ru.semenov.stepSevenTesting.entity.Student;

@Path(StudentResource.STUDENT_RESOURCE_URI)
public class StudentResource {
    @Inject
    private StudentService studentService;
    public static final String STUDENT_RESOURCE_URI = "/api/v1/students/withExceptionMapper";
    @GET
    @Path("/{studentId}")
    @Transactional
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getStudent(@PathParam("studentId") String studentId) throws BusinessException {
        Student student = studentService.getStudent(studentId);
        if(student == null){
            return Response.status(Response.Status.BAD_REQUEST).build();
        }else{
            return Response.ok(student).build();
        }
    }

    @POST
    @Path("/addStudent")
    public Response addStudent(@RequestBody StudentDTO studentDTO){
        Student student = studentService.createStudent(studentDTO);
        return Response.status(Response.Status.OK)
                .entity(student)
                .build();
    }

    @GET
    @Path("/divide/{i}")
    @Transactional
    public Response testApi(@PathParam("i") int i) throws TechnicalException {
        try{
            int out = 8/i;
            return Response.ok("Output after dividing "+out).build();
        }catch (Exception e){
           throw new TechnicalException(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(),
                   e.getMessage());
        }
    }
}