package ru.semenov.restEasy.exception;

import io.quarkus.arc.All;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@Builder
@AllArgsConstructor
@ToString
public class ErrorMessageMovie {
    @NotBlank(message = "Status is required")
    private int status;
    @Size(min = 1, max = 100, message = "Message must be between 1 and 100 characters")
    private String message;
}
