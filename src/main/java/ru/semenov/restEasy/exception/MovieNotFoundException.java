package ru.semenov.restEasy.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MovieNotFoundException extends RuntimeException {
    Integer status;
    public MovieNotFoundException(String s, Integer status) {
        super(s);
        this.status = status;
    }
}
