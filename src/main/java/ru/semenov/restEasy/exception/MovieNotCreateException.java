package ru.semenov.restEasy.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MovieNotCreateException extends Throwable {
    Integer status;
    public MovieNotCreateException(String s, Integer status) {
        super(s);
        this.status = status;
    }
}
