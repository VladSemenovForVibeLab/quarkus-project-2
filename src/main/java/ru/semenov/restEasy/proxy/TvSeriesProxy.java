package ru.semenov.restEasy.proxy;

//import jakarta.ws.rs.*;
//import jakarta.ws.rs.core.MediaType;
//import jakarta.ws.rs.core.Response;
//import org.eclipse.microprofile.metrics.annotation.Counted;
//import org.eclipse.microprofile.metrics.annotation.Metered;
//import org.eclipse.microprofile.metrics.annotation.Timed;
//import org.eclipse.microprofile.openapi.annotations.Operation;
//import org.eclipse.microprofile.openapi.annotations.media.Content;
//import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
//import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
//import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

//@Path(TvSeriesProxy.TV_SERIES_PROXY_URL)
//@Produces(MediaType.APPLICATION_JSON)
//@RegisterRestClient(baseUri = TvSeriesProxy.BASE_URI)
//public interface TvSeriesProxy {
//    String BASE_URI = "http://api/tvmaze.com/";
//    String TV_SERIES_PROXY_URL ="/singlesearch/";
//
//    @GET
//    @Produces(MediaType.APPLICATION_JSON)
//    @Path("/shows")
//    @Counted(
//            name = "getRestClient_counter",
//            description = "Счетчик вызовов метода getRestClient"
//    )
//    @Timed(
//            name = "getRestClient_timer",
//            description = "Таймер замеров времени выполнения метода getRestClient"
//    )
//    @Metered(
//            name = "getRestClient_meter",
//            description = "Счетчик метрик вызовов метода getRestClient"
//    )
//    @Operation(
//            operationId = "getRestClient",
//            summary = "Все программы tv c другого сайта",
//            description = "Получение всех программ"
//    )
//    @APIResponse(
//            responseCode = "200",
//            description = "Программы получены",
//            content = @Content(mediaType = MediaType.APPLICATION_JSON)
//    )
//     Response getRestClient(
//            @Parameter(
//                    description = "g - параметр передаваемый на другой сайт -> String",
//                    required = true,
//                    example = "game"
//            )
//            @QueryParam("g") String title);
//}
