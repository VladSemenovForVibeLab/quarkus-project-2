package ru.semenov.restEasy.service;

import ru.semenov.restEasy.entity.TvSeries;

import java.util.List;

public interface TvSeriesService {
    List<TvSeries> getAll(int pageNo, int pageOfSize);
}
