package ru.semenov.restEasy.service;

import ru.semenov.restEasy.dto.request.MovieRequestDTOForUpdate;
import ru.semenov.restEasy.dto.request.MovieRequestDTOForCreate;
import ru.semenov.restEasy.entity.Movie;
import ru.semenov.restEasy.exception.MovieNotCreateException;

import java.util.List;

public interface MovieService {
    List<Movie> getAll(int pageNo, int pageOfSize);

    Long getSizeMovieList();

    Movie createNewMovie(MovieRequestDTOForCreate movieRequestDTOForCreate) throws MovieNotCreateException;

    Movie updateMovie(MovieRequestDTOForUpdate movieRequestDTOForUpdate);

    Movie getMovieById(String id);

    boolean deleteMovie(Movie movieForDelete);
}
