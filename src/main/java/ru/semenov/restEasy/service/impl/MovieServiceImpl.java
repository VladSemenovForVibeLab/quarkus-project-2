package ru.semenov.restEasy.service.impl;

import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.panache.common.Page;
import io.vertx.mutiny.pgclient.PgPool;
import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.transaction.Transactional;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validator;
import ru.semenov.restEasy.dto.request.MovieRequestDTOForUpdate;
import ru.semenov.restEasy.dto.request.MovieRequestDTOForCreate;
import ru.semenov.restEasy.entity.Movie;
import ru.semenov.restEasy.exception.MovieNotCreateException;
import ru.semenov.restEasy.repository.MovieRepository;
import ru.semenov.restEasy.service.MovieService;
import ru.semenov.restEasy.util.request.MovieRequestDTOForCreateUtil;
import ru.semenov.restEasy.util.request.MovieRequestDTOForUpdateUtil;

import java.util.List;
import java.util.Set;

@Singleton
public class MovieServiceImpl implements MovieService {
    private final MovieRepository movieRepository;
    private final MovieRequestDTOForCreateUtil movieRequestDTOForCreateUtil;

    private final MovieRequestDTOForUpdateUtil movieRequestDTOForUpdateUtil;
    private final Validator validator;


    @Inject
    public MovieServiceImpl(MovieRepository movieRepository, MovieRequestDTOForCreateUtil movieRequestDTOForCreateUtil, MovieRequestDTOForUpdateUtil movieRequestDTOForUpdateUtil, Validator validator) {
        this.movieRepository = movieRepository;
        this.movieRequestDTOForCreateUtil = movieRequestDTOForCreateUtil;
        this.movieRequestDTOForUpdateUtil = movieRequestDTOForUpdateUtil;
        this.validator = validator;
    }

    @Override
    public List<Movie> getAll(int pageNo, int pageOfSize) {
        PanacheQuery<Movie> all = movieRepository.findAll();
        List<Movie> movieList = all.page(Page.of(pageNo,pageOfSize)).list();
        return movieList;
    }

    @Override
    public Long getSizeMovieList() {
        Long size = movieRepository.getSizeMovieListRepository();
        return size;
    }

    @Override
    @Transactional
    public Movie createNewMovie(MovieRequestDTOForCreate movieRequestDTOForCreate) throws MovieNotCreateException {
        if(isValidateEntity(movieRequestDTOForCreate)){
            Movie dao = movieRequestDTOForCreateUtil.toEntity(movieRequestDTOForCreate);
            movieRepository.persist(dao);
            return dao;
        }else{
            throw new MovieNotCreateException("Movie not created!",Integer.valueOf(404));
        }
    }

    @Override
    @Transactional
    public Movie updateMovie(MovieRequestDTOForUpdate movieRequestDTOForUpdate) {
            Movie existingMovie = movieRepository.findById(movieRequestDTOForUpdate.getId());
            Movie setMovie = setField(movieRequestDTOForUpdate,existingMovie);
            movieRepository.persist(setMovie);
            return setMovie;
    }

    @Override
    public Movie getMovieById(String id) {
        Movie movie = movieRepository.findById(id);
        return movie;
    }

    @Override
    @Transactional
    public boolean deleteMovie(Movie movieForDelete) {
        movieRepository.delete(movieForDelete);
        return true;
    }

    private Movie setField(MovieRequestDTOForUpdate movieRequestDTOForUpdate, Movie existingMovie) {
        existingMovie.setActors(movieRequestDTOForUpdate.getActors());
        existingMovie.setDirector(movieRequestDTOForUpdate.getDirector());
        existingMovie.setGenre(movieRequestDTOForUpdate.getGenre());
        existingMovie.setReleaseYear(movieRequestDTOForUpdate.getReleaseYear());
        existingMovie.setTitle(movieRequestDTOForUpdate.getTitle());
        return existingMovie;
    }

    public boolean isValidateEntity(MovieRequestDTOForCreate entity) {
        Set<ConstraintViolation<MovieRequestDTOForCreate>> validate = validator.validate(entity);
        if (validate.isEmpty()) {
            return true;
        }
        return false;
    }
}
