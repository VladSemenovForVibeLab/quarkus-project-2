package ru.semenov.restEasy.service.impl;

import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.panache.common.Page;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
//import org.eclipse.microprofile.rest.client.inject.RestClient;
import ru.semenov.restEasy.entity.TvSeries;
//import ru.semenov.restEasy.proxy.TvSeriesProxy;
import ru.semenov.restEasy.repository.TvSeriesRepository;
import ru.semenov.restEasy.service.TvSeriesService;

import java.util.List;
@ApplicationScoped
public class TvSeriesServiceImpl implements TvSeriesService {
    private final TvSeriesRepository repository;
//    @RestClient
//    private  TvSeriesProxy proxy;

    @Inject
    public TvSeriesServiceImpl(TvSeriesRepository repository) {
        this.repository = repository;
    }

    @Override
    @Transactional
    public List<TvSeries> getAll(int pageNo, int pageOfSize) {
//        Response response = proxy.getRestClient("game of thrones");
//        repository.persist((TvSeries) response.getEntity());
//        List<TvSeries> all = (List<TvSeries>) repository.findAll();
//        return all;
        PanacheQuery<TvSeries> all = repository.findAll();
        List<TvSeries> tvSeries = all.page(Page.of(pageNo,pageOfSize)).list();
        return tvSeries;
    }
}
