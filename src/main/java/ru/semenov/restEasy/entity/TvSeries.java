package ru.semenov.restEasy.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.*;
import lombok.*;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.validator.constraints.Length;
import ru.semenov.restEasy.listener.TvSeriesListener;

import java.io.Serializable;
import java.net.URL;
import java.util.Set;

@Entity(name = TvSeries.ENTITY_NAME)
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Table(name = TvSeries.TABLE_NAME,
        uniqueConstraints = {
                @UniqueConstraint(name = "UNIQUE_TV_SERIES_ID_NAME_URL",
                        columnNames = {
                                "id",
                                "name",
                                "url"
                        })
        },
        indexes = {
                @Index(name = "IND_TV_SERIES_ID",
                        columnList =
                                "id")
        }
)
@Getter(lazy = false,
        value = AccessLevel.PUBLIC)
@Setter(value = AccessLevel.PUBLIC)
@NoArgsConstructor
@AllArgsConstructor
@ToString(
        callSuper = true,
        includeFieldNames = true,
        doNotUseGetters = false,
        onlyExplicitlyIncluded = true)
@EqualsAndHashCode(
        cacheStrategy = EqualsAndHashCode.CacheStrategy.LAZY,
        callSuper = false,
        doNotUseGetters = false,
        onlyExplicitlyIncluded = true)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(
        name = "type",
        discriminatorType = DiscriminatorType.INTEGER)
@DiscriminatorValue(value = "1")
@NamedQueries({
        @NamedQuery(name = "TvSeries.findAll", query = "SELECT t FROM TvSeries t"),
        @NamedQuery(name = "TvSeries.findById", query = "SELECT t FROM TvSeries t WHERE t.id = :id"),
        @NamedQuery(name = "TvSeries.findByName", query = "SELECT t FROM TvSeries t WHERE t.name = :name"),
        @NamedQuery(name = "TvSeries.findByGenre", query = "SELECT t FROM TvSeries t WHERE :genre MEMBER OF t.genres"),
        @NamedQuery(name = "TvSeries.findByLanguage", query = "SELECT t FROM TvSeries t WHERE t.language = :language")
})
@NamedNativeQueries({
        @NamedNativeQuery(
                name = "TvSeries.findAllNative",
                query = "SELECT * FROM tv_series",
                resultClass = TvSeries.class
        ),
        @NamedNativeQuery(
                name = "TvSeries.findByIdNative",
                query = "SELECT * FROM tv_series WHERE id = :id",
                resultClass = TvSeries.class
        ),
        @NamedNativeQuery(
                name = "TvSeries.findByNameNative",
                query = "SELECT * FROM tv_series WHERE name = :name",
                resultClass = TvSeries.class
        ),
})
@NamedEntityGraphs({
        @NamedEntityGraph(
                name = "TvSeries.full",
                attributeNodes = {
                        @NamedAttributeNode("id"),
                        @NamedAttributeNode("name"),
                        @NamedAttributeNode("url"),
                        @NamedAttributeNode("summary"),
                        @NamedAttributeNode("language"),
                        @NamedAttributeNode("genres"),
                        @NamedAttributeNode("officialSite")
                }
        ),
        @NamedEntityGraph(
                name = "TvSeries.tvSeriesWithGenres",
                attributeNodes = {
                        @NamedAttributeNode("name"),
                        @NamedAttributeNode("genres")
                }),
        @NamedEntityGraph(
                name = "TvSeries.tvSeriesWithDetails",
                attributeNodes = {
                        @NamedAttributeNode("name"),
                        @NamedAttributeNode("url"),
                        @NamedAttributeNode("summary"),
                        @NamedAttributeNode("language"),
                        @NamedAttributeNode("officialSite")
                })

})
@NamedStoredProcedureQuery(
        name = "TvSeries.updateTvSeriesDetails",
        procedureName = "update_tv_series_details", // Название хранимой процедуры
        parameters = {
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "tvSeriesId", type = Long.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "newLanguage", type = String.class),
                @StoredProcedureParameter(mode = ParameterMode.IN, name = "newSummary", type = String.class)
        }
)
@Builder
@EntityListeners({TvSeriesListener.class})
public class TvSeries implements Serializable, Cloneable, Comparable<TvSeries> {
    public static final String ENTITY_NAME = "TvSeries";
    public static final String TABLE_NAME = "tv_series";
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_NAME = "name";
    private static final String COLUMN_URL = "url";
    private static final String COLUMN_SUMMARY = "summary";
    private static final String COLUMN_LANGUAGE = "language";
    private static final String COLUMN_GENRE = "genre";
    private static final String COLUMN_OFFICIAL_SITE = "official_site";
    @ToString.Include
    @EqualsAndHashCode.Include
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter(AccessLevel.NONE)
    @Column(name = COLUMN_ID, nullable = false, unique = true, insertable = false, updatable = false)
    private Long id;
    @ToString.Include
    @EqualsAndHashCode.Include
    @Column(name = COLUMN_NAME, nullable = false, unique = true,columnDefinition = "VARCHAR(255)")
    @Basic(optional = false,fetch = FetchType.EAGER)
    @NotBlank(message = "Название не должно быть пустым!")
    @NotNull(message = "Название не должно быть null!")
    @Size(min = 3,max = 300)
    @Pattern(regexp = "[A-Za-z0-9]+")
    @Length(min = 3,max = 300,message = "Название должно быть в диапазоне от 3 до 300 символов")
    private String name;
    @ToString.Include
    @EqualsAndHashCode.Include
    @Column(name = COLUMN_URL, nullable = false, unique = true,columnDefinition = "VARCHAR(255)")
    @Basic(optional = false,fetch = FetchType.EAGER)
    @Size(min = 5, max = 3000)
    @org.hibernate.validator.constraints.URL
    @NotNull
    private URL url;
    @Lob
    @Column(name = COLUMN_SUMMARY, nullable = false, unique = false,columnDefinition = "VARCHAR(255)",length = 300)
    @Basic(optional = false,fetch = FetchType.EAGER)
    @ToString.Include
    @NotBlank(message = "Описание не может быть пустым!")
    @NotNull(message = "Описание не может быть null!")
    @Size(min = 3,max = 300)
    @Length(min = 3,max = 300,message = "Описание должен быть в диапазоне от 3 до 300 символов")
    private String summary;
    @Column(name = COLUMN_LANGUAGE, nullable = false, unique = false,columnDefinition = "VARCHAR(255)",length = 100)
    @Basic(optional = false,fetch = FetchType.EAGER)
    @ToString.Include
    @NotBlank(message = "Язык не может быть пустым!")
    @NotNull(message = "Язык не может быть null!")
    @Size(min = 3,max = 100)
    @Length(min = 3,max = 100,message = "Описание должен быть в диапазоне от 3 до 100 символов")
    private String language;
    @ElementCollection
    @CollectionTable(name = "tv_series_genres", joinColumns = @JoinColumn(name = "tv_series_id"))
    @OrderColumn
    @Column(name = COLUMN_GENRE, nullable = false, unique = false,columnDefinition = "VARCHAR(255)")
    @Basic(optional = false,fetch = FetchType.EAGER)
    @ToString.Include
    @NotBlank(message = "Жанр не может быть пустым!")
    @NotNull(message = "Жанр не может быть null!")
    @NotEmpty(message = "Список жанров не может быть пустым")
    @Size(min = 3,max = 300)
    @Length(min = 3,max = 300,message = "Жанр должен быть в диапазоне от 3 до 300 символов")
    private Set<String> genres;
    @ToString.Include
    @EqualsAndHashCode.Include
    @Column(name = COLUMN_OFFICIAL_SITE, nullable = false, unique = false,columnDefinition = "VARCHAR(255)")
    @Basic(optional = false,fetch = FetchType.EAGER)
    @Size(min = 5, max = 3000)
    @org.hibernate.validator.constraints.URL
    @NotNull
    private URL officialSite;

    @Override
    public int compareTo(TvSeries o) {
        return this.name.compareTo(o.name);
    }
}
