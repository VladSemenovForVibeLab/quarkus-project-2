package ru.semenov.restEasy.entity;

import jakarta.persistence.*;
import jakarta.validation.Valid;
import jakarta.validation.constraints.*;
import lombok.*;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UuidGenerator;
import org.hibernate.validator.constraints.Length;
import ru.semenov.restEasy.listener.MovieListener;

import java.io.Serializable;
import java.util.List;
@Entity(name = Movie.ENTITY_NAME)
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Table(name = Movie.TABLE_NAME,
        uniqueConstraints = {
                @UniqueConstraint(name = "UNIQUE_MOVIE_TITLE_ID",
                        columnNames = {
                                "id",
                                "title"
                        })
        },
        indexes = {
                @Index(name = "IND_MOVIE_ID",
                        columnList =
                                "id")
        }
)
@Getter(lazy = false,
        value = AccessLevel.PUBLIC)
@Setter(value = AccessLevel.PUBLIC)
@NoArgsConstructor
@AllArgsConstructor
@ToString(
        callSuper = true,
        includeFieldNames = true,
        doNotUseGetters = false,
        onlyExplicitlyIncluded = true)
@EqualsAndHashCode(
        cacheStrategy = EqualsAndHashCode.CacheStrategy.LAZY,
        callSuper = false,
        doNotUseGetters = false,
        onlyExplicitlyIncluded = true)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(
        name = "type",
        discriminatorType = DiscriminatorType.INTEGER)
@DiscriminatorValue(value = "1")
@NamedQueries({
        @NamedQuery(name = "Movie.findAll", query = "SELECT m FROM Movie m"),
        @NamedQuery(name = "Movie.findByTitle", query = "SELECT m FROM Movie m WHERE m.title = :title"),
        @NamedQuery(name = "Movie.findByDirector", query = "SELECT m FROM Movie m WHERE m.director = :director"),
        @NamedQuery(name = "Movie.findByGenre", query = "SELECT m FROM Movie m WHERE m.genre = :genre"),
        @NamedQuery(name = "Movie.findByReleaseYear", query = "SELECT m FROM Movie m WHERE m.releaseYear = :release_year"),
        @NamedQuery(name = "Movie.findByActor", query = "SELECT m FROM Movie m JOIN m.actors a WHERE a = :actor")
})
@NamedNativeQueries({
        @NamedNativeQuery(name = "Movie.findAllNative", query = "SELECT * FROM movie", resultClass = Movie.class),
        @NamedNativeQuery(name = "Movie.findByTitleNative", query = "SELECT * FROM movie WHERE title = :title", resultClass = Movie.class),
        @NamedNativeQuery(name = "Movie.findByDirectorNative", query = "SELECT * FROM movie WHERE director = :director", resultClass = Movie.class),
        @NamedNativeQuery(name = "Movie.findByGenreNative", query = "SELECT * FROM movie WHERE genre = :genre", resultClass = Movie.class),
        @NamedNativeQuery(name = "Movie.findByReleaseYearNative", query = "SELECT * FROM movie WHERE release_year = :release_year", resultClass = Movie.class),
        @NamedNativeQuery(name = "Movie.findByActorNative", query = "SELECT m.* FROM movie m JOIN movie_actor ma ON m.id = ma.movie_id JOIN actor a ON ma.actor_id = a.id WHERE a.name = :actor", resultClass = Movie.class)
})
@NamedEntityGraph(
        name = "MovieWithActors",
        attributeNodes = {
                @NamedAttributeNode("actors")
        }
)
@Builder
@EntityListeners({MovieListener.class})
public class Movie implements Serializable,Cloneable,Comparable<Movie> {
    public static final String ENTITY_NAME = "Movie";
    public static final String TABLE_NAME = "movie";
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_TITLE = "title";
    private static final String COLUMN_DIRECTOR = "director";
    private static final String COLUMN_GENRE = "genre";
    private static final String COLUMN_YEAR = "release_year";
    @ToString.Include
    @EqualsAndHashCode.Include
    @Id
    @UuidGenerator
    @GeneratedValue(strategy = GenerationType.UUID, generator = "system-uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Setter(AccessLevel.NONE)
    @Column(name = COLUMN_ID, nullable = false, unique = true, insertable = false, updatable = false)
    private String id;
    @Column(name = COLUMN_TITLE, nullable = false, unique = true,columnDefinition = "VARCHAR(255)")
    @Basic(optional = false,fetch = FetchType.EAGER)
    @ToString.Include
    @NotBlank(message = "Название не должно быть пустым!")
    @EqualsAndHashCode.Include
    @NotNull(message = "Название не должно быть null!")
    @Size(min = 3,max = 300)
    @Length(min = 3,max = 300,message = "Название должно быть в диапазоне от 3 до 300 символов")
    private String title;
    @Column(name = COLUMN_DIRECTOR, nullable = false, unique = false,columnDefinition = "VARCHAR(255)")
    @Basic(optional = false,fetch = FetchType.EAGER)
    @ToString.Include
    @NotBlank(message = "Директор не может быть пустым!")
    @NotNull(message = "Директор не может быть null!")
    @Size(min = 3,max = 300)
    @Length(min = 3,max = 300,message = "Имя директора должно быть в диапазоне от 3 до 300 символов")
    private String director;
    @Column(name = COLUMN_GENRE, nullable = false, unique = false,columnDefinition = "VARCHAR(255)")
    @Basic(optional = false,fetch = FetchType.EAGER)
    @ToString.Include
    @NotBlank(message = "Жанр не может быть пустым!")
    @NotNull(message = "Жанр не может быть null!")
    @Size(min = 3,max = 300)
    @Length(min = 3,max = 300,message = "Жанр должен быть в диапазоне от 3 до 300 символов")
    private String genre;
    @Column(name = COLUMN_YEAR, nullable = false, unique = false)
    @Basic(optional = false,fetch = FetchType.EAGER)
    @ToString.Include
    @NotNull(message = "Год не может быть null!")
    @Min(value = 1800, message = "Год должен быть не менее 1800")
    @Max(value = 3000, message = "Год не может быть более 3000")
    private Integer releaseYear;
    @ElementCollection
    @CollectionTable(name = "movie_actors", joinColumns = @JoinColumn(name = "movie_id"))
    @OrderColumn
    @Column(name = "")
    @NotNull(message = "Список актеров не может быть null")
    @NotEmpty(message = "Список актеров не может быть пустым")
    @Size(min = 1, message = "Список актеров должен содержать хотя бы одного актера")
    private List<@Valid String> actors;

    @Override
    public int compareTo(Movie o) {
        return this.releaseYear.compareTo(o.releaseYear);
    }
    @Override
    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            throw new InternalError(e.toString());
        }
    }
}
