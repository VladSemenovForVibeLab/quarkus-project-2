package ru.semenov.restEasy.mapper;

import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;
import ru.semenov.exceptionHandler.dto.ErrorMessage;
import ru.semenov.microserviceHibernate.exception.ProductNotFoundException;
import ru.semenov.restEasy.exception.ErrorMessageMovie;
import ru.semenov.restEasy.exception.MovieNotFoundException;

@Provider
public class MovieNotFoundExceptionMapper implements ExceptionMapper<MovieNotFoundException> {

    @Override
    public Response toResponse(MovieNotFoundException e) {
        ErrorMessageMovie errorMessageMovie = new ErrorMessageMovie();
        errorMessageMovie.setStatus(e.getStatus());
        errorMessageMovie.setMessage(e.getMessage());
        return Response
                .status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity(errorMessageMovie)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }
}
