package ru.semenov.restEasy.resource;

import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.core.Response;
import ru.semenov.restEasy.dto.request.MovieRequestDTOForUpdate;
import ru.semenov.restEasy.dto.request.MovieRequestDTOForCreate;
import ru.semenov.restEasy.exception.MovieNotCreateException;

public interface MovieResource {
    Response getAllPMovie( int pageNo,
                          int pageOfSize);
    Response getSizeMovieList();
    Response addNewMovie( MovieRequestDTOForCreate movieRequestDTOForCreate) throws MovieNotCreateException;
    Response updateMovie(MovieRequestDTOForUpdate movieRequestDTOForUpdate);
    Response deleteMovie(String movieId);
    Response getMovieById( String id);
}

