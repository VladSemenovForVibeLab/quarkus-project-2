package ru.semenov.restEasy.resource;

import jakarta.ws.rs.core.Response;

public interface TvSeriesResource {
    Response get(int pageNo,int pageOfSize);
}
