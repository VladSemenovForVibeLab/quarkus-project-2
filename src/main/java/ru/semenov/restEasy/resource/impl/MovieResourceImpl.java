package ru.semenov.restEasy.resource.impl;

import jakarta.inject.Inject;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.eclipse.microprofile.faulttolerance.Fallback;
import org.eclipse.microprofile.metrics.annotation.Counted;
import org.eclipse.microprofile.metrics.annotation.Metered;
import org.eclipse.microprofile.metrics.annotation.Timed;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import ru.semenov.restEasy.dto.request.MovieRequestDTOForUpdate;
import ru.semenov.restEasy.dto.request.MovieRequestDTOForCreate;
import ru.semenov.restEasy.dto.response.MovieResponseDTO;
import ru.semenov.restEasy.entity.Movie;
import ru.semenov.restEasy.exception.MovieNotCreateException;
import ru.semenov.restEasy.resource.MovieResource;
import ru.semenov.restEasy.service.MovieService;
import ru.semenov.restEasy.util.response.MovieResponseDTOUtil;

import java.util.ArrayList;
import java.util.List;

@Path(MovieResourceImpl.MOVIE_RESOURCE_URI)
@Tag(name = "Кино контроллер", description = "Основные ендпоинты для работы с фильмами")
public class MovieResourceImpl implements MovieResource {
    public static final String MOVIE_RESOURCE_URI = "/api/v1/movies";
    private final MovieService movieService;
    private final MovieResponseDTOUtil movieResponseDTOUtil;

    @Inject
    public MovieResourceImpl(MovieService movieService, MovieResponseDTOUtil movieResponseDTOUtil) {
        this.movieService = movieService;
        this.movieResponseDTOUtil = movieResponseDTOUtil;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{pageNo}/{pageOfSize}")
    @Counted(
            name = "getAllPMovie_counter",
            description = "Счетчик вызовов метода getAllPMovie"
    )
    @Timed(
            name = "getAllPMovie_timer",
            description = "Таймер замеров времени выполнения метода getAllPMovie"
    )
    @Metered(
            name = "getAllPMovie_meter",
            description = "Счетчик метрик вызовов метода getAllPMovie"
    )
    @Operation(
            operationId = "getAllPMovie",
            summary = "Все фильмы",
            description = "Получение всех филмов с пагинацией"
    )
    @APIResponse(
            responseCode = "200",
            description = "Фильмы получены",
            content = @Content(mediaType = MediaType.APPLICATION_JSON)
    )
// for RestClient method
//    @Fallback(fallbackMethod = "fallbackGet")
    @Override
    public Response getAllPMovie(
            @Parameter(
                    description = "pageNo - номер страницы которая будет отображена",
                    required = true,
                    example = "0"
            )
            @PathParam("pageNo") int pageNo,
            @Parameter(
                    description = "pageOfSize - количество отображеемой информации на странице",
                    required = true,
                    example = "1"
            )
            @PathParam("pageOfSize") int pageOfSize) {
        List<Movie> all = movieService.getAll(pageNo, pageOfSize);
        List<MovieResponseDTO> responseDTOS = movieResponseDTOUtil.toDTOList(all);
        return Response.ok(responseDTOS).build();
    }
    public Response fallbackGet() {
        return Response.ok(new ArrayList<>()).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Counted(
            name = "getSizeMovieList_counter",
            description = "Счетчик вызовов метода getSizePMovieList"
    )
    @Timed(
            name = "getSizeMovieList_timer",
            description = "Таймер замеров времени выполнения метода getSizePMovieList"
    )
    @Metered(
            name = "getSizeMovieList_meter",
            description = "Счетчик метрик вызовов метода getSizePMovieList"
    )
    @Operation(
            operationId = "getSizeMovieList",
            summary = "Количество всех фильмов",
            description = "Получение всех филмов -> Long -> количество"
    )
    @APIResponse(
            responseCode = "200",
            description = "Фильмы получены",
            content = @Content(mediaType = MediaType.APPLICATION_JSON)
    )
    @Override
    public Response getSizeMovieList() {
        Long size = movieService.getSizeMovieList();
        return Response.ok(size).build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Counted(
            name = "addNewMovie_counter",
            description = "Счетчик вызовов метода addNewMovie"
    )
    @Timed(
            name = "addNewMovie_timer",
            description = "Таймер замеров времени выполнения метода addNewMovie"
    )
    @Metered(
            name = "addNewMovie_meter",
            description = "Счетчик метрик вызовов метода addNewMovie"
    )
    @Operation(
            operationId = "addNewMovie",
            summary = "Добавление новых фильмов",
            description = "Отправка DTO и получение DTO в ответ -> отправленное сохраняется в БД"
    )
    @APIResponse(
            responseCode = "200",
            description = "Фильм сохранен",
            content = @Content(mediaType = MediaType.APPLICATION_JSON)
    )
    @Override
    public Response addNewMovie(@RequestBody(
            description = "DTO для создания нового фильма",
            required = true,
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = MovieRequestDTOForCreate.class))) MovieRequestDTOForCreate movieRequestDTOForCreate) throws MovieNotCreateException {
        Movie movie = movieService.createNewMovie(movieRequestDTOForCreate);
        MovieResponseDTO dto = movieResponseDTOUtil.toDTO(movie);
        return Response.ok(dto).build();
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Counted(
            name = "updateMovie_counter",
            description = "Счетчик вызовов метода updateMovie"
    )
    @Timed(
            name = "updateMovie_timer",
            description = "Таймер замеров времени выполнения метода updateMovie"
    )
    @Metered(
            name = "updateMovie_meter",
            description = "Счетчик метрик вызовов метода updateMovie"
    )
    @Operation(
            operationId = "updateMovie",
            summary = "Обновление старых фильмов",
            description = "Отправка DTO и получение DTO в ответ -> отправленное обновляется в БД по id"
    )
    @APIResponse(
            responseCode = "200",
            description = "Фильм обновлен",
            content = @Content(mediaType = MediaType.APPLICATION_JSON)
    )
    @Override
    public Response updateMovie(@RequestBody(
            description = "DTO для обновления уже существующего фильма",
            required = true,
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = MovieRequestDTOForUpdate.class))
    ) MovieRequestDTOForUpdate movieRequestDTOForUpdate) {
        Movie response = movieService.updateMovie(movieRequestDTOForUpdate);
        MovieResponseDTO dto = movieResponseDTOUtil.toDTO(response);
        return Response.ok(dto).build();
    }

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Counted(
            name = "deleteMovie_counter",
            description = "Счетчик вызовов метода deleteMovie"
    )
    @Timed(
            name = "deleteMovie_timer",
            description = "Таймер замеров времени выполнения метода deleteMovie"
    )
    @Metered(
            name = "deleteMovie_meter",
            description = "Счетчик метрик вызовов метода deleteMovie"
    )
    @Operation(
            operationId = "deleteMovie",
            summary = "Удаление старых фильмов",
            description = "Отправка id и получение boolean в ответ -> отправленное удаляется в БД по id"
    )
    @APIResponse(
            responseCode = "200",
            description = "Фильм удален",
            content = @Content(mediaType = MediaType.APPLICATION_JSON)
    )
    @Override
    public Response deleteMovie(
            @Parameter(
                    description = "id фильма, который нужно удалить",
                    example = "8904473c-56f7-4483-8e16-1d80a18bb14e",
                    required = true
            )
            @PathParam("id") String id) {
        Movie movieForDelete = movieService.getMovieById(id);
        boolean isDeleted = movieService.deleteMovie(movieForDelete);
        return Response.ok(isDeleted).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Counted(
            name = "getMovieById_counter",
            description = "Счетчик вызовов метода getMovieById"
    )
    @Timed(
            name = "getMovieById_timer",
            description = "Таймер замеров времени выполнения метода getMovieById"
    )
    @Metered(
            name = "getMovieById_meter",
            description = "Счетчик метрик вызовов метода getMovieById"
    )
    @Path("/{id}")
    @Operation(
            operationId = " getMovieById",
            summary = "Нахождение старых фильмов",
            description = "Отправка id и получение DTO в ответ -> отправленное находится в БД по id"
    )
    @APIResponse(
            responseCode = "200",
            description = "Фильм найден",
            content = @Content(mediaType = MediaType.APPLICATION_JSON)
    )
    @Override
    public Response getMovieById(@PathParam("id") String id) {
        Movie movieById = movieService.getMovieById(id);
        if (movieById == null) {
            return Response.status(HttpServletResponse.SC_NOT_FOUND).build();
        }
        MovieResponseDTO dto = movieResponseDTOUtil.toDTO(movieById);
        return Response.ok(dto).build();
    }

}
