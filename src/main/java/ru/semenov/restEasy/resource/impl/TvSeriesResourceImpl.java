package ru.semenov.restEasy.resource.impl;

import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.eclipse.microprofile.metrics.annotation.Counted;
import org.eclipse.microprofile.metrics.annotation.Metered;
import org.eclipse.microprofile.metrics.annotation.Timed;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import ru.semenov.restEasy.dto.response.TvSeriesResponseDTO;
import ru.semenov.restEasy.entity.TvSeries;
import ru.semenov.restEasy.resource.TvSeriesResource;
import ru.semenov.restEasy.service.TvSeriesService;
import ru.semenov.restEasy.util.response.TvSeriesResponseDTOUtil;

import java.util.List;

@Tag(name = "Контроллер с телевизионными программами", description = "Основные ендпоинты для работы с программами")
@Path(TvSeriesResourceImpl.TV_SERIES_RESOURCE_URI)
public class TvSeriesResourceImpl implements TvSeriesResource {
    private final TvSeriesService tvSeriesService;
    private final TvSeriesResponseDTOUtil tvSeriesResponseDTOUtil;
    public static final String TV_SERIES_RESOURCE_URI = "/api/v1/tv-series";

    @Inject
    public TvSeriesResourceImpl(TvSeriesService tvSeriesService, TvSeriesResponseDTOUtil tvSeriesResponseDTOUtil) {
        this.tvSeriesService = tvSeriesService;
        this.tvSeriesResponseDTOUtil = tvSeriesResponseDTOUtil;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{pageNo}/{pageOfSize}")
    @Counted(
            name = "get_counter",
            description = "Счетчик вызовов метода get"
    )
    @Timed(
            name = "get_timer",
            description = "Таймер замеров времени выполнения метода get"
    )
    @Metered(
            name = "get_meter",
            description = "Счетчик метрик вызовов метода get"
    )
    @Operation(
            operationId = "get",
            summary = "Все программы tv",
            description = "Получение всех программ с пагинацией"
    )
    @APIResponse(
            responseCode = "200",
            description = "Программы получены",
            content = @Content(mediaType = MediaType.APPLICATION_JSON)
    )
    @Override
    public Response get(
            @Parameter(
                    description = "pageNo - номер страницы которая будет отображена",
                    required = true,
                    example = "0"
            )
            @PathParam("pageNo") int pageNo,
            @Parameter(
                    description = "pageOfSize - количество отображеемой информации на странице",
                    required = true,
                    example = "1"
            )
            @PathParam("pageOfSize") int pageOfSize) {
        List<TvSeries> all = tvSeriesService.getAll(pageNo, pageOfSize);
        List<TvSeriesResponseDTO> responseDTOS = tvSeriesResponseDTOUtil.toDTOList(all);
        return Response.ok(responseDTOS).build();
    }
}
