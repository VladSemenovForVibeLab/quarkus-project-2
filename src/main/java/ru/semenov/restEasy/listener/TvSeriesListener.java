package ru.semenov.restEasy.listener;

import jakarta.persistence.*;
import ru.semenov.restEasy.entity.Movie;
import ru.semenov.restEasy.entity.TvSeries;

public class TvSeriesListener {


    @PrePersist
    public void prePersist(Object object) {
        if (object instanceof Movie) {
            TvSeries tvSeries = (TvSeries) object;
            System.out.println("Программа будет успешно сохранена в базе данных:");
            System.out.println("ID: " + tvSeries.getId());
            System.out.println("Название: " + tvSeries.getName());
            System.out.println("URL: " + tvSeries.getUrl());
            System.out.println("Описание: " + tvSeries.getSummary());
            System.out.println("Язык: " + tvSeries.getLanguage());
            System.out.println("Жанр: " + tvSeries.getGenres());
            System.out.println("Оффициальный сайт: " + tvSeries.getOfficialSite());
        }
    }

    @PostPersist
    public void postPersist(Object object) {
        if (object instanceof Movie) {
            TvSeries tvSeries = (TvSeries) object;
            System.out.println("Программа была успешно сохранена в базе данных:");
            System.out.println("ID: " + tvSeries.getId());
            System.out.println("Название: " + tvSeries.getName());
            System.out.println("URL: " + tvSeries.getUrl());
            System.out.println("Описание: " + tvSeries.getSummary());
            System.out.println("Язык: " + tvSeries.getLanguage());
            System.out.println("Жанр: " + tvSeries.getGenres());
            System.out.println("Оффициальный сайт: " + tvSeries.getOfficialSite());
        }
    }

    @PreUpdate
    public void preUpdate(Object object) {
        if (object instanceof Movie) {
            TvSeries tvSeries = (TvSeries) object;
            System.out.println("Программа будет успешно обновлена в базе данных:");
            System.out.println("ID: " + tvSeries.getId());
            System.out.println("Название: " + tvSeries.getName());
            System.out.println("URL: " + tvSeries.getUrl());
            System.out.println("Описание: " + tvSeries.getSummary());
            System.out.println("Язык: " + tvSeries.getLanguage());
            System.out.println("Жанр: " + tvSeries.getGenres());
            System.out.println("Оффициальный сайт: " + tvSeries.getOfficialSite());
        }
    }

    @PostUpdate
    public void postUpdate(Object object) {
        if (object instanceof Movie) {
            TvSeries tvSeries = (TvSeries) object;
            System.out.println("Программа была успешно обновлена в базе данных:");
            System.out.println("ID: " + tvSeries.getId());
            System.out.println("Название: " + tvSeries.getName());
            System.out.println("URL: " + tvSeries.getUrl());
            System.out.println("Описание: " + tvSeries.getSummary());
            System.out.println("Язык: " + tvSeries.getLanguage());
            System.out.println("Жанр: " + tvSeries.getGenres());
            System.out.println("Оффициальный сайт: " + tvSeries.getOfficialSite());
        }
    }

    @PreRemove
    public void preRemove(Object object) {
        if (object instanceof Movie) {
            TvSeries tvSeries = (TvSeries) object;
            System.out.println("Программа будет успешно удалена в базе данных:");
            System.out.println("ID: " + tvSeries.getId());
            System.out.println("Название: " + tvSeries.getName());
            System.out.println("URL: " + tvSeries.getUrl());
            System.out.println("Описание: " + tvSeries.getSummary());
            System.out.println("Язык: " + tvSeries.getLanguage());
            System.out.println("Жанр: " + tvSeries.getGenres());
            System.out.println("Оффициальный сайт: " + tvSeries.getOfficialSite());
        }
    }

    @PostRemove
    public void postRemove(Object object) {
        if (object instanceof Movie) {
            TvSeries tvSeries = (TvSeries) object;
            System.out.println("Программа была успешно удалена в базе данных:");
            System.out.println("ID: " + tvSeries.getId());
            System.out.println("Название: " + tvSeries.getName());
            System.out.println("URL: " + tvSeries.getUrl());
            System.out.println("Описание: " + tvSeries.getSummary());
            System.out.println("Язык: " + tvSeries.getLanguage());
            System.out.println("Жанр: " + tvSeries.getGenres());
            System.out.println("Оффициальный сайт: " + tvSeries.getOfficialSite());
        }
    }

    @PostLoad
    public void postLoad(Object object) {
        if (object instanceof Movie) {
            TvSeries tvSeries = (TvSeries) object;
            System.out.println("Программа была успешно загружена из базы данных:");
            System.out.println("ID: " + tvSeries.getId());
            System.out.println("Название: " + tvSeries.getName());
            System.out.println("URL: " + tvSeries.getUrl());
            System.out.println("Описание: " + tvSeries.getSummary());
            System.out.println("Язык: " + tvSeries.getLanguage());
            System.out.println("Жанр: " + tvSeries.getGenres());
            System.out.println("Оффициальный сайт: " + tvSeries.getOfficialSite());
        }
    }
}
