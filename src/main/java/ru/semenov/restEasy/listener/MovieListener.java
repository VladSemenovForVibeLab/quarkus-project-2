package ru.semenov.restEasy.listener;

import jakarta.persistence.*;
import ru.semenov.restEasy.entity.Movie;
public class MovieListener {


    @PrePersist
    public void prePersist(Object object) {
        if (object instanceof Movie) {
            Movie movie = (Movie) object;
            System.out.println("Фильм будет успешно сохранен в базе данных:");
            System.out.println("Название: " + movie.getTitle());
            System.out.println("Режиссер: " + movie.getDirector());
            System.out.println("Жанр: " + movie.getGenre());
            System.out.println("Год выпуска: " + movie.getReleaseYear());
            System.out.println("Актеры: " + movie.getActors());
        }
    }

    @PostPersist
    public void postPersist(Object object) {
        if (object instanceof Movie) {
            Movie movie = (Movie) object;
            System.out.println("Фильм был успешно сохранен в базе данных:");
            System.out.println("Название: " + movie.getTitle());
            System.out.println("Режиссер: " + movie.getDirector());
            System.out.println("Жанр: " + movie.getGenre());
            System.out.println("Год выпуска: " + movie.getReleaseYear());
            System.out.println("Актеры: " + movie.getActors());
        }
    }

    @PreUpdate
    public void preUpdate(Object object) {
        if (object instanceof Movie) {
            Movie movie = (Movie) object;
            System.out.println("Фильм будет успешно обновлен в базе данных:");
            System.out.println("Название: " + movie.getTitle());
            System.out.println("Режиссер: " + movie.getDirector());
            System.out.println("Жанр: " + movie.getGenre());
            System.out.println("Год выпуска: " + movie.getReleaseYear());
            System.out.println("Актеры: " + movie.getActors());
        }
    }

    @PostUpdate
    public void postUpdate(Object object) {
        if (object instanceof Movie) {
            Movie movie = (Movie) object;
            System.out.println("Фильм был успешно изменен в базе данных:");
            System.out.println("Название: " + movie.getTitle());
            System.out.println("Режиссер: " + movie.getDirector());
            System.out.println("Жанр: " + movie.getGenre());
            System.out.println("Год выпуска: " + movie.getReleaseYear());
            System.out.println("Актеры: " + movie.getActors());
        }
    }

    @PreRemove
    public void preRemove(Object object) {
        if (object instanceof Movie) {
            Movie movie = (Movie) object;
            System.out.println("Фильм будет успешно удален в базе данных:");
            System.out.println("Название: " + movie.getTitle());
            System.out.println("Режиссер: " + movie.getDirector());
            System.out.println("Жанр: " + movie.getGenre());
            System.out.println("Год выпуска: " + movie.getReleaseYear());
            System.out.println("Актеры: " + movie.getActors());
        }
    }

    @PostRemove
    public void postRemove(Object object) {
        if (object instanceof Movie) {
            Movie movie = (Movie) object;
            System.out.println("Фильм был успешно удален в базе данных:");
            System.out.println("Название: " + movie.getTitle());
            System.out.println("Режиссер: " + movie.getDirector());
            System.out.println("Жанр: " + movie.getGenre());
            System.out.println("Год выпуска: " + movie.getReleaseYear());
            System.out.println("Актеры: " + movie.getActors());
        }
    }

    @PostLoad
    public void postLoad(Object object) {
        if (object instanceof Movie) {
            Movie movie = (Movie) object;
            System.out.println("Фильм был успешно загружен в базе данных:");
            System.out.println("Название: " + movie.getTitle());
            System.out.println("Режиссер: " + movie.getDirector());
            System.out.println("Жанр: " + movie.getGenre());
            System.out.println("Год выпуска: " + movie.getReleaseYear());
            System.out.println("Актеры: " + movie.getActors());
        }
    }
}
