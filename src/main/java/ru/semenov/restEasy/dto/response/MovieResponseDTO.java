package ru.semenov.restEasy.dto.response;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import ru.semenov.restEasy.entity.Movie;

import java.util.List;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class MovieResponseDTO {
    @Schema(name = "id", description = "ID фильма")
    @NotBlank(message = "Id is required")
    private String id;
    @Schema(name = "title", description = "The title of the movie response", defaultValue = "Сорвиголова Кик Бутовски")
    @NotBlank(message = "Title is required")
    @NotEmpty(message = "Название фильма не может быть пустым")
    private String title;
    @NotBlank(message = "Director is required")
    @Schema(name = "director", description = "The director of the movie response", defaultValue = "Сандро Корсаро")
    @NotEmpty(message = "Режиссер фильма не может быть пустым")
    private String director;
    @NotBlank(message = "Genre is required")
    @Schema(name = "genre", description = "The genre of the movie response", defaultValue = "приключения, " +
            "школьная жизнь, " +
            "боевик, " +
            "комедия")
    private String genre;
    @NotNull(message = "Год выхода фильма не может быть пустым")
    @Min(value = 1800, message = "Год выхода фильма должен быть не меньше 1800")
    @Schema(name = "year",description = "The year",defaultValue = " 2010")
    private Integer releaseYear;
    @NotEmpty(message = "Actors are required")
    @Schema(name = "actors",description = "The actors",defaultValue = "[\"Чарли Шлэттер\", "+
            "\"Мэтт Л. Джон\", " +
            "\"Дэнни Кукси\"]")
    private List< String> actors;
    public MovieResponseDTO(Movie movie){
        this.id=movie.getId();
        this.title=movie.getTitle();
        this.director=movie.getDirector();
        this.genre=movie.getGenre();
        this.releaseYear=movie.getReleaseYear();
        this.actors=movie.getActors();
    }
}
