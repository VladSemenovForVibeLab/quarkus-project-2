package ru.semenov.restEasy.dto.response;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import lombok.*;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import ru.semenov.restEasy.entity.TvSeries;

import java.net.URL;
import java.util.Set;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class TvSeriesResponseDTO {
    @Schema(name = "id", description = "ID программы")
    @NotBlank(message = "Id is required")
    private Long id;
    @Schema(name = "name", description = "The name of the TvSeries response", defaultValue = "Сорвиголова Кик Бутовски")
    @NotBlank(message = "name is required")
    @NotEmpty(message = "Название фильма не может быть пустым")
    private String name;
    @NotBlank(message = "url is required")
    @Schema(name = "url", description = "The url of the tvSeries response")
    @NotEmpty(message = "URL программы не может быть пустым")
    private URL url;
    @Schema(name = "summary", description = "The summary of the tvSeries response")
    @NotBlank(message = "summary is required")
    @NotEmpty(message = "Описание программы не может быть пустым")
    private String summary;
    @Schema(name = "language", description = "The language of the tvSeries response")
    @NotBlank(message = "language is required")
    @NotEmpty(message = "Язык программы не может быть пустым")
    private String language;
    @NotBlank(message = "Genres is required")
    @Schema(name = "genres", description = "The genre of the movie response", defaultValue = "приключения, " +
            "школьная жизнь, " +
            "боевик, " +
            "комедия")
    private Set<String> genres;
    @NotBlank(message = "url officialSite is required")
    @Schema(name = "officialSite", description = "The officialSite of the tvSeries response")
    @NotEmpty(message = "Сайт программы не может быть пустым")
    private URL officialSite;

    public TvSeriesResponseDTO(TvSeries tvSeries){
        this.id=tvSeries.getId();
        this.name=tvSeries.getName();
        this.url=tvSeries.getUrl();
        this.officialSite=tvSeries.getOfficialSite();
        this.summary=tvSeries.getSummary();
        this.language=tvSeries.getLanguage();
        this.genres=tvSeries.getGenres();
    }
}
