package ru.semenov.restEasy.dto.request;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import ru.semenov.restEasy.entity.Movie;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Schema(name = MovieRequestDTOForCreate.SCHEMA_NAME,
        description = "Схема для запроса на создание фильма")
public class MovieRequestDTOForCreate {
    public static final String SCHEMA_NAME = "MovieRequestDTOForCreate";
    @Schema(name = "title", description = "The title of the movie request", defaultValue = "Сорвиголова Кик Бутовски", required = true)
    @NotBlank(message = "Title is required")
    private String title;
    @NotBlank(message = "Director is required")
    @Schema(name = "director", description = "The director of the movie request", defaultValue = "Сандро Корсаро", required = true)
    private String director;
    @NotBlank(message = "Genre is required")
    @Schema(name = "genre", description = "The genre of the movie request", defaultValue = "приключения, " +
            "школьная жизнь, " +
            "боевик, " +
            "комедия",
            required = true)
    private String genre;
    @NotNull(message = "releaseYear is required")
    @Schema(name = "releaseYear", description = "The releaseYear", defaultValue = " 2010", required = true)
    private Integer releaseYear;
    @NotEmpty(message = "Actors are required")
    @Schema(name = "actors", description = "The actors", defaultValue = "[\"Чарли Шлэттер\", " +
            "\"Мэтт Л. Джон\", " +
            "\"Дэнни Кукси\"]")
    private List<String> actors;

    public MovieRequestDTOForCreate(Movie movie) {
        this.title = movie.getTitle();
        this.director = movie.getDirector();
        this.genre = movie.getGenre();
        this.releaseYear = movie.getReleaseYear();
        this.actors = movie.getActors();
    }
}
