package ru.semenov.restEasy.dto.request;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import ru.semenov.restEasy.entity.Movie;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Schema(name = MovieRequestDTOForUpdate.SCHEMA_NAME,
        description = "Схема на запрос для изменения уже существующего фильма!")
public class MovieRequestDTOForUpdate {
    public static final String SCHEMA_NAME = "MovieRequestDTOForUpdate";
    @Schema(name = "id", description = "ID фильма", required = true)
    @NotBlank(message = "Id is required")
    private String id;
    @Schema(name = "title", description = "The title of the movie request", defaultValue = "Сорвиголова Кик Бутовски", required = true)
    @NotBlank(message = "Title is required")
    private String title;
    @NotBlank(message = "Director is required")
    @Schema(name = "director", description = "The director of the movie request", defaultValue = "Сандро Корсаро", required = true)
    private String director;
    @NotBlank(message = "Genre is required")
    @Schema(name = "genre", description = "The genre of the movie request", defaultValue = "приключения, " +
            "школьная жизнь, " +
            "боевик, " +
            "комедия",
            required = true)
    private String genre;
    @NotNull(message = "releaseYear is required")
    @Schema(name = "releaseYear", description = "The releaseYear", defaultValue = " 2010", required = true)
    private Integer releaseYear;
    @NotEmpty(message = "Actors are required")
    @Schema(name = "actors", description = "The actors", defaultValue = "[\"Чарли Шлэттер\", " +
            "\"Мэтт Л. Джон\", " +
            "\"Дэнни Кукси\"]",
            required = true)
    private List<String> actors;

    public MovieRequestDTOForUpdate(Movie movie) {
        this.id = movie.getId();
        this.title = movie.getTitle();
        this.director = movie.getDirector();
        this.genre = movie.getGenre();
        this.releaseYear = movie.getReleaseYear();
        this.actors = movie.getActors();
    }
}
