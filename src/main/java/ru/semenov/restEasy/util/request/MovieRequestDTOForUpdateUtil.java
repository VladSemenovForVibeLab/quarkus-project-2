package ru.semenov.restEasy.util.request;

import jakarta.enterprise.context.ApplicationScoped;
import ru.semenov.restEasy.dto.request.MovieRequestDTOForCreate;
import ru.semenov.restEasy.dto.request.MovieRequestDTOForUpdate;
import ru.semenov.restEasy.entity.Movie;
import ru.semenov.restEasy.util.EntityDTOUtil;

import java.util.List;

@ApplicationScoped
public class MovieRequestDTOForUpdateUtil implements EntityDTOUtil<Movie, MovieRequestDTOForUpdate> {
    @Override
    public Movie toEntity(MovieRequestDTOForUpdate dto) {
        return Movie
                .builder()
                .id(dto.getId())
                .actors(dto.getActors())
                .genre(dto.getGenre())
                .title(dto.getTitle())
                .releaseYear(dto.getReleaseYear())
                .director(dto.getDirector())
                .actors(dto.getActors())
                .build();
    }

    @Override
    public MovieRequestDTOForUpdate toDTO(Movie entity) {
        return new MovieRequestDTOForUpdate(entity);
    }

    @Override
    public List<Movie> toEntityList(List<MovieRequestDTOForUpdate> dtoList) {
        return dtoList.stream().map(this::toEntity).toList();
    }

    @Override
    public List<MovieRequestDTOForUpdate> toDTOList(List<Movie> entityList) {
        return entityList.stream().map(this::toDTO).toList();
    }
}
