package ru.semenov.restEasy.util.request;

import jakarta.enterprise.context.ApplicationScoped;
import ru.semenov.restEasy.dto.request.MovieRequestDTOForCreate;
import ru.semenov.restEasy.entity.Movie;
import ru.semenov.restEasy.util.EntityDTOUtil;

import java.util.List;
@ApplicationScoped
public class MovieRequestDTOForCreateUtil implements EntityDTOUtil<Movie, MovieRequestDTOForCreate> {
    @Override
    public Movie toEntity(MovieRequestDTOForCreate dto) {
        return Movie
                .builder()
                .actors(dto.getActors())
                .genre(dto.getGenre())
                .title(dto.getTitle())
                .releaseYear(dto.getReleaseYear())
                .director(dto.getDirector())
                .actors(dto.getActors())
                .build();
    }

    @Override
    public MovieRequestDTOForCreate toDTO(Movie entity) {
        return new MovieRequestDTOForCreate(entity);
    }

    @Override
    public List<Movie> toEntityList(List<MovieRequestDTOForCreate> dtoList) {
        return dtoList.stream().map(this::toEntity).toList();
    }

    @Override
    public List<MovieRequestDTOForCreate> toDTOList(List<Movie> entityList) {
        return entityList.stream().map(this::toDTO).toList();
    }
}
