package ru.semenov.restEasy.util;

import java.util.List;

public interface EntityDTOUtil<E,D> {
    E toEntity(D dto);
    D toDTO(E entity);
    List<E> toEntityList(List<D> dtoList);
    List<D> toDTOList(List<E> entityList);
}
