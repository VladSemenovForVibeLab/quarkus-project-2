package ru.semenov.restEasy.util.response;

import jakarta.enterprise.context.ApplicationScoped;
import ru.semenov.restEasy.dto.response.TvSeriesResponseDTO;
import ru.semenov.restEasy.entity.TvSeries;
import ru.semenov.restEasy.util.EntityDTOUtil;

import java.util.List;
@ApplicationScoped
public class TvSeriesResponseDTOUtil implements EntityDTOUtil<TvSeries, TvSeriesResponseDTO> {
    @Override
    public TvSeries toEntity(TvSeriesResponseDTO dto) {
        return TvSeries
                .builder()
                .summary(dto.getSummary())
                .url(dto.getUrl())
                .id(dto.getId())
                .genres(dto.getGenres())
                .language(dto.getLanguage())
                .name(dto.getName())
                .officialSite(dto.getOfficialSite())
                .build();
    }

    @Override
    public TvSeriesResponseDTO toDTO(TvSeries entity) {
        return new TvSeriesResponseDTO(entity);
    }

    @Override
    public List<TvSeries> toEntityList(List<TvSeriesResponseDTO> dtoList) {
        return dtoList.stream().map(this::toEntity).toList();
    }

    @Override
    public List<TvSeriesResponseDTO> toDTOList(List<TvSeries> entityList) {
        return entityList.stream().map(this::toDTO).toList();
    }
}
