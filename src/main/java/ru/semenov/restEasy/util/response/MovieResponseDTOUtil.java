package ru.semenov.restEasy.util.response;

import jakarta.enterprise.context.ApplicationScoped;
import ru.semenov.restEasy.dto.response.MovieResponseDTO;
import ru.semenov.restEasy.entity.Movie;
import ru.semenov.restEasy.util.EntityDTOUtil;

import java.util.List;
@ApplicationScoped
public class MovieResponseDTOUtil implements EntityDTOUtil<Movie, MovieResponseDTO> {
    @Override
    public Movie toEntity(MovieResponseDTO dto) {
        return Movie
                .builder()
                .id(dto.getId())
                .title(dto.getTitle())
                .genre(dto.getGenre())
                .actors(dto.getActors())
                .releaseYear(dto.getReleaseYear())
                .director(dto.getDirector())
                .build();
    }

    @Override
    public MovieResponseDTO toDTO(Movie entity) {
        return new MovieResponseDTO(entity);
    }

    @Override
    public List<Movie> toEntityList(List<MovieResponseDTO> dtoList) {
        return dtoList.stream().map(this::toEntity).toList();
    }

    @Override
    public List<MovieResponseDTO> toDTOList(List<Movie> entityList) {
        return entityList.stream().map(this::toDTO).toList();
    }
}
