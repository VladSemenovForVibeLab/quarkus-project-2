package ru.semenov.restEasy.application;

import org.eclipse.microprofile.openapi.annotations.OpenAPIDefinition;
import org.eclipse.microprofile.openapi.annotations.info.Info;
import org.eclipse.microprofile.openapi.annotations.info.License;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

@OpenAPIDefinition(
        info = @Info(
                title = "Приложение с основным функционалом QUARKUS",
                description = "приложение в котором можно работать по CRUD",
                version = "0.0.1",
                license = @License(
                        name = "MIT",
                        url = "http://localhost:8080"
                )
        ),
        tags = {
                @Tag(
                        name = "Операции CRUD",description = "Основная библиотека для меня!"
                )
        }
)
public class Application extends jakarta.ws.rs.core.Application {
}
