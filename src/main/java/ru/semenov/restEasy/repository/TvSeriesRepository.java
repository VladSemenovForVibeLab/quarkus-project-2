package ru.semenov.restEasy.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;
import ru.semenov.restEasy.entity.TvSeries;

@ApplicationScoped
public class TvSeriesRepository implements PanacheRepository<TvSeries> {
}
