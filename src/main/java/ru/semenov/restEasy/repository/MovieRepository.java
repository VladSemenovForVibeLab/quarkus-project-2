package ru.semenov.restEasy.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import ru.semenov.restEasy.entity.Movie;

@ApplicationScoped
public class MovieRepository implements PanacheRepository<Movie> {

    public Movie findById(String id) {
        return find("id",id).firstResult();
    }

    public Long getSizeMovieListRepository() {
        return count();
    }
}
