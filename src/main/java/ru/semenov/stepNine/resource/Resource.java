package ru.semenov.stepNine.resource;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.eclipse.microprofile.config.inject.ConfigProperty;

@Path("/configuration_profile")
public class Resource {
    @ConfigProperty(name = "CEO_property",defaultValue = "CodeHouse")
    private String ceo;

    @ConfigProperty(name = "profile",defaultValue = "NONE")
    private String profile;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/ceo_name")
    public Response getCEOName(){
        return Response.ok(ceo + profile).build();
    }
}
