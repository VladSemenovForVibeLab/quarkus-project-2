package ru.semenov.mapStruct.service;

import ru.semenov.mapStruct.dto.CitizenDTO;
import ru.semenov.mapStruct.entity.Citizens;

public interface CitizensService {
    Citizens createCitizen(CitizenDTO citizenDTO);

    long getCitizenCount();

    Citizens findById(String id);
}
