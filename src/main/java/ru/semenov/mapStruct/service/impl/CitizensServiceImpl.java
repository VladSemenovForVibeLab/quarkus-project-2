package ru.semenov.mapStruct.service.impl;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.transaction.Transactional;
import ru.semenov.mapStruct.dto.CitizenDTO;
import ru.semenov.mapStruct.entity.Citizens;
import ru.semenov.mapStruct.mapper.CitizensMapper;
import ru.semenov.mapStruct.repository.CitizensRepository;
import ru.semenov.mapStruct.service.CitizensService;
@Singleton
public class CitizensServiceImpl implements CitizensService {

    private final CitizensMapper citizensMapper;
    private final CitizensRepository citizensRepository;

    @Inject
    public CitizensServiceImpl(CitizensMapper citizensMapper, CitizensRepository citizensRepository) {
        this.citizensMapper = citizensMapper;
        this.citizensRepository = citizensRepository;
    }

    @Override
    @Transactional
    public Citizens createCitizen(CitizenDTO citizenDTO) {
        Citizens dao = citizensMapper.toDAO(citizenDTO);
        citizensRepository.saveCitizen(
                dao.getFirstName(),
                dao.getLastName(),
                dao.getGender(),
                dao.getAge(),
                dao.getSalary(),
                dao.getAddress());
        return citizensMapper.toDAO(citizenDTO);
    }

    @Override
    public long getCitizenCount() {
        return citizensRepository.count();
    }

    @Override
    public Citizens findById(String id) {
        Citizens entityFind = citizensRepository.findById(id);
        return entityFind;
    }
}
