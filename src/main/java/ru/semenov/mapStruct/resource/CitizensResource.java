package ru.semenov.mapStruct.resource;

import jakarta.inject.Inject;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.eclipse.microprofile.metrics.annotation.Counted;
import org.eclipse.microprofile.metrics.annotation.Gauge;
import org.eclipse.microprofile.metrics.annotation.Metered;
import org.eclipse.microprofile.metrics.annotation.Timed;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import ru.semenov.mapStruct.dto.CitizenDTO;
import ru.semenov.mapStruct.entity.Citizens;
import ru.semenov.mapStruct.service.CitizensService;

import javax.xml.validation.Validator;

@Path(CitizensResource.CITIZENS_RESOURCE_URI)
public class CitizensResource {
    public static final String CITIZENS_RESOURCE_URI="/api/v1/citizens";
    private final CitizensService service;


    @Inject
    public CitizensResource(CitizensService service) {
        this.service = service;
    }

    @POST
    @Path("/addNewCitizen")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Counted(
            name = "addNewCitizen_counter",
            description = "Счетчик вызовов метода addNewCitizen"
    )
    @Timed(
            name = "addNewCitizen_timer",
            description = "Таймер замеров времени выполнения метода addNewCitizen"
    )
    @Metered(
            name = "addNewCitizen_meter",
            description = "Счетчик метрик вызовов метода addNewCitizen"
    )
    public Response createCitizen(@RequestBody CitizenDTO citizenDTO){
        return Response.ok(service.createCitizen(citizenDTO)).build();
    }

    @Gauge(
            name = "citizen_count",
            unit = "citizens",
            description = "Количество граждан"
    )
    public long getCitizenCount(){
        return service.getCitizenCount();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Counted(
            name = "getCitizen_counter",
            description = "Счетчик вызовов метода getCitizen"
    )
    @Timed(
            name = "getCitizen_timer",
            description = "Таймер замеров времени выполнения метода getCitizen"
    )
    @Metered(
            name = "getCitizen_meter",
            description = "Счетчик метрик вызовов метода getCitizen"
    )
    public Response findCitizens(@PathParam("id") String id){
        Citizens entity = service.findById(id);
        if(entity==null){
            return Response.status(HttpServletResponse.SC_NOT_FOUND).build();
        }
        return Response.ok(entity).build();
    }
}
