package ru.semenov.mapStruct.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.persistence.Table;
import jakarta.transaction.Transactional;
import org.jboss.logging.annotations.Param;
import org.jboss.resteasy.annotations.Query;
import ru.semenov.mapStruct.entity.Citizens;

import java.math.BigDecimal;

@ApplicationScoped
public class CitizensRepository implements PanacheRepository<Citizens> {
    public Citizens findById(String id){
        return find("id",id).firstResult();
    }
    public long count(){
        return count();
    }

    @Transactional
    public Citizens saveCitizen(String firstName, String lastName, String gender, Byte age, BigDecimal salary, String address){
        Citizens citizen = new Citizens();
        citizen.setFirstName(firstName);
        citizen.setLastName(lastName);
        citizen.setGender(gender);
        citizen.setAge(age);
        citizen.setSalary(salary);
        citizen.setAddress(address);
        persist(citizen);
        return citizen;
    }

}
