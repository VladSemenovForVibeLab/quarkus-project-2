package ru.semenov.mapStruct.listener;

import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import ru.semenov.mapStruct.entity.Citizens;

import java.math.BigDecimal;

public class CitizensListener {
    @PrePersist
    public void prePersist(Citizens entity) {
        if(entity.getAge()<18){
            throw new RuntimeException("Cannot save citizen under the age of 18");
        }else {
            System.out.println("New citizen is being saved");
        }
    }
    @PreUpdate
    public void preUpdate(Citizens entity) {
        BigDecimal newSalary = entity.getSalary().multiply(new BigDecimal("1.1"));
        entity.setSalary(newSalary);
        System.out.println("Citizen salary is being updated...");
    }
}
