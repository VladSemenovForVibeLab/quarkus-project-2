package ru.semenov.mapStruct.dto;


import lombok.*;

import java.math.BigDecimal;
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class CitizenDTO {
    private String firstName;
    private String lastName;
    private String gender;
    private Byte age;
    private BigDecimal salary;
    private String address;
}
