package ru.semenov.mapStruct.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.semenov.mapStruct.dto.CitizenDTO;
import ru.semenov.mapStruct.entity.Citizens;

@Mapper(
        componentModel = "cdi"
)
public interface CitizensMapper {
    @Mapping(target = "firstName",expression = "java(citizenDTO.getFirstName())")
    @Mapping(target = "lastName",expression = "java(citizenDTO.getLastName())")
    @Mapping(target = "address",expression = "java(citizenDTO.getAddress())")
    @Mapping(target = "age",expression = "java(citizenDTO.getAge())")
    @Mapping(target = "gender",expression = "java(citizenDTO.getGender())")
    @Mapping(target = "salary",expression = "java(citizenDTO.getSalary())")
    Citizens toDAO(CitizenDTO citizenDTO);

    @Mapping(target = "firstName",expression = "java(citizenDTO.getFirstName())")
    @Mapping(target = "lastName",expression = "java(citizenDTO.getLastName())")
    @Mapping(target = "address",expression = "java(citizenDTO.getAddress())")
    @Mapping(target = "age",expression = "java(citizenDTO.getAge())")
    @Mapping(target = "gender",expression = "java(citizenDTO.getGender())")
    @Mapping(target = "salary",expression = "java(citizenDTO.getSalary())")
    CitizenDTO toDTO(Citizens citizens);
}
