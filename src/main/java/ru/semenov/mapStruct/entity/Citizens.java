package ru.semenov.mapStruct.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import jakarta.ws.rs.DefaultValue;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UuidGenerator;
import ru.semenov.hibernateReferences.entity.Citizen;
import ru.semenov.mapStruct.listener.CitizensListener;

import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Table(name = Citizen.TABLE_NAME)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "TYPE", discriminatorType = DiscriminatorType.INTEGER)
@NamedQueries({
        @NamedQuery(name = "Citizens.getAllCitizens",
                query = "SELECT c FROM Citizens c"),
        @NamedQuery(name = "Citizens.getCitizenById",
                query = "SELECT c FROM Citizens c WHERE c.id = :id"),
        @NamedQuery(name = "Citizens.getCitizensByLastName",
                query = "SELECT c FROM Citizens c WHERE c.lastName = :lastName"),
        @NamedQuery(name = "Citizens.getCitizensByAddress",
                query = "SELECT c FROM Citizens c WHERE c.address = :address")
})
@NamedNativeQueries({
        @NamedNativeQuery(name = "Citizens.getAllMales",
        query = "SELECT * FROM citizens WHERE gender = 'male'",
        resultClass = Citizens.class),
        @NamedNativeQuery(name = "Citizens.getAllFemales",
        query = "SELECT * FROM citizens WHERE gender = 'female'",
        resultClass = Citizens.class),
        @NamedNativeQuery(name = "Citizens.getByFirstName",
        query = "SELECT * FROM citizens WHERE firstName = :firstName",
        resultClass = Citizens.class)
})
@NamedEntityGraph(
        name = "graph.Citizens.withAddress",
        attributeNodes = {
                @NamedAttributeNode("address")
        }
)
@EntityListeners(CitizensListener.class)
public class Citizens implements Serializable,Cloneable,Comparable<Citizens> {
    public static final String TABLE_NAME = "citizens";
    private static final String COLUMN_ID="id";
    private static final String COLUMN_FIRST_NAME="first_name";
    private static final String COLUMN_LAST_NAME="last_name";
    private static final String COLUMN_ADDRESS="address";
    private static final String COLUMN_GENDER="gender";
    private static final String COLUMN_AGE ="age";
    private static final String COLUMN_SALARY ="salary";
    @Id
    @NotNull
    @UuidGenerator
    @Setter(AccessLevel.NONE)
    @GeneratedValue(strategy = GenerationType.UUID,generator = "system-uuid")
    @GenericGenerator(name = "uuid",strategy = "uuid2")
    @Column(name = COLUMN_ID,nullable = false,unique = true)
    @Basic(optional = false,fetch = FetchType.EAGER)
    private String id;
    @Column(name = COLUMN_FIRST_NAME,nullable = true,unique = false)
    @DefaultValue("username")
    @Basic(optional = true,fetch = FetchType.EAGER)
    private String firstName;
    @Column(name = COLUMN_LAST_NAME,nullable = true,unique = false)
    @Basic(optional = true,fetch = FetchType.EAGER)
    @DefaultValue("lastname")
    private String lastName;
    @Column(name = COLUMN_ADDRESS,nullable = false,unique = false)
    @Basic(optional = false,fetch = FetchType.EAGER)
    private String address;
    @Column(name = COLUMN_GENDER,nullable = true,unique = false)
    @Basic(optional = true,fetch = FetchType.LAZY)
    @DefaultValue("Non-binary")
    private String gender;
    @Column(name = COLUMN_AGE,nullable = false,unique = false)
    @Basic(optional = false,fetch = FetchType.LAZY)
    @Min(value = 18,message = "Min age should be 18 year")
    @Max(value = 130,message = "Max age should be 130 years")
    private Byte age;
    @Column(name = COLUMN_SALARY,nullable = true,unique = false)
    @Basic(optional = true,fetch = FetchType.LAZY)
    private BigDecimal salary;
    @Override
    public int compareTo(Citizens o) {
        return this.age.compareTo(o.age);
    }
    @Override
    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            throw new InternalError(e.toString());
        }
    }
}
