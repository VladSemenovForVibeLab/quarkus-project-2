package ru.semenov.stepSevenTesting.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;
import ru.semenov.stepSevenTesting.entity.Student;

import java.util.List;

@ApplicationScoped
public class StudentRepository implements PanacheRepository<Student> {
    public Student findByIdString(String id) {
        return find("id",id).firstResult();
    }

    public List<Student> getStudentListByBranch(String branch){
        return list("Select s from Student s where s.branch = ?1",branch);
    }
}
