package ru.semenov.stepSevenTesting.resource;

import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import ru.semenov.stepSevenTesting.entity.Student;
import ru.semenov.stepSevenTesting.repository.StudentRepository;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

@Path("/students")
//@Consumes(MediaType.APPLICATION_JSON)
//@Produces(MediaType.APPLICATION_JSON)
public class StudentResource {

    @Inject
    private StudentRepository studentRepository;

    @GET
    @Path("/getStudentList")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getStudentList() {
        List<Student> students = studentRepository.listAll();
        return Response.ok(students).build();
    }

    @GET
    @Path("/getStudentListCS")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCSStudentList() throws Exception {
        List<Student> studentsCS = new ArrayList<>();
        List<Student> students = studentRepository.listAll();
        students.forEach(s -> {
            if (s.getBranch().equalsIgnoreCase("CS")) {
                studentsCS.add(s);
            }
        });
        return Response.ok(students).build();
    }

    @POST
    @Transactional
    @Path("/addStudent")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addStudent(@RequestBody Student student) {
        studentRepository.persist(student);
        if (studentRepository.isPersistent(student)) {
            return Response.created(URI.create("/students/" + student.getId())).build();
        }
        return Response.ok(Response.status(Response.Status.BAD_REQUEST)).build();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getStudentById(@PathParam("id") String id) {
        Student student = studentRepository.findByIdString(id);
        if (student == null) {
            return Response.ok(Response.status(Response.Status.NOT_FOUND)).build();
        } else {
            return Response.ok(student).build();
        }
    }

    @POST
    @Path("/update/{id}")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response updateStudent(@PathParam("id") String id,
                                  @RequestBody Student studentUpdate) {
        Student student = studentRepository.findByIdString(id);
        if (student != null) {
            student.setName(studentUpdate.getName());
            return Response.ok(student).build();
        } else {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }
}
