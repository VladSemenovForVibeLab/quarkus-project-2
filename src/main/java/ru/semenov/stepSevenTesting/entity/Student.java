package ru.semenov.stepSevenTesting.entity;

import jakarta.persistence.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UuidGenerator;

import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = Student.TABLE_NAME)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type",discriminatorType = DiscriminatorType.INTEGER)
public class Student implements Serializable,Cloneable,Comparable<Student> {
    public static final String TABLE_NAME ="student";
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_NAME = "name";
    private static final String COLUMN_BRANCH="branch";
    @Id
    @UuidGenerator
    @GeneratedValue(strategy = GenerationType.UUID,generator = "system-uuid")
    @GenericGenerator(name = "uuid",strategy = "uuid2")
    @Column(name = COLUMN_ID,nullable = false,unique = true)
    private String id;

    @Basic(optional = false,fetch = FetchType.EAGER)
    @Column(name = COLUMN_NAME,nullable = false)
    private String name;
    @Basic(optional = true,fetch = FetchType.LAZY)
    @Column(name = COLUMN_BRANCH,nullable = true)
    private String branch;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return Objects.equals(id, student.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBranch() {
        return branch;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", branch='" + branch + '\'' +
                '}';
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public Student(String id, String name, String branch) {
        this.id = id;
        this.name = name;
        this.branch = branch;
    }

    public Student() {
    }

    @Override
    public int compareTo(Student o) {
        return this.name.compareTo(o.name);
    }
    @Override
    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            throw new InternalError(e.toString());
        }
    }
}
