package ru.semenov.stepik.cache;

import io.quarkus.cache.CacheManager;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

//Для очистки всех кэшей микросервиса можно воспользоваться классом CacheManager:
@ApplicationScoped
public class CacheClearer {

    @Inject
    CacheManager cacheManager;

    public void clearAllCaches() {
        for (String cacheName : cacheManager.getCacheNames()) {
            cacheManager
                    .getCache(cacheName)
                    .get()
                    .invalidateAll()
                    .await()
                    .indefinitely();
        }
    }
}
