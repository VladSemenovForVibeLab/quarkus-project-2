package ru.semenov.stepik.cache;

import io.quarkus.cache.CacheInvalidate;
import io.quarkus.cache.CacheInvalidateAll;
import io.quarkus.cache.CacheKey;
import io.quarkus.cache.CacheResult;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class MyTestService {

    /**
     * В данном примере идет кэширование результата метода getSum для входных параметров a и b. Т. е. если мы вызвали метод для значений 3 и 5 впервые, то он будет работать около 2 секунд. При повторном вызове он вернет результат мгновенно из кэша, т.к. тело метода выполняться не будет. cacheName = "sum-cache" означает, что значения кэша будут ассоциированы с определенным наименованием (потребуется далее для сброса кэша и т. д.).
     * <p>
     * Важно! Аннотации кэширования не работают на private методах!
     *
     * @param a
     * @param b
     * @return
     */
    @CacheResult(cacheName = "sum-cache")
    public Long getSum(Long a, Long b) {
        try {
            Thread.sleep(2000L);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        return a + b;
    }

    /**
     * @param keyElement1
     * @param keyElement2
     * @return
     * @CacheResult - пытается найти результат работы метода в кэше в зависимости от входных параметров метода. Если входной параметр не является примитивным, у него должны быть реализованы методы equals() и hashCode() для корректного расчета хэш суммы.
     */
    @CacheResult(cacheName = "foo")
    public Object load(String keyElement1, Integer keyElement2) {
        return null;
        // выполняем код метода, возвращаем результат
    }

    /**
     * @param keyElement2
     * @param keyElement1
     * @CacheInvalidate - удаляет одну запись из кэша с определенным наименованием. Запись находится через входные параметры метода.
     */
    @CacheInvalidate(cacheName = "foo")
    public void invalidate(String keyElement2, Integer keyElement1) {
    }

    /**
     * @CacheInvalidateAll - удаляет все записи из кэша с определенным наименованием.
     */
    @CacheInvalidateAll(cacheName = "foo")
    public void invalidateAll() {
    }

    /**
     * @param keyElement1
     * @param keyElement2
     * @param notPartOfTheKey
     * @return
     * @CacheKey - добавляется к параметрам метода, позволяет задавать, какие входные параметры учавствуют в формировании кэша. Если аннотации нет, параметры используются все.
     */
    @CacheResult(cacheName = "foo")
    public String load(@CacheKey Object keyElement1, @CacheKey Object keyElement2, Object notPartOfTheKey) {
        return null;// тело метода, возвращение результата
    }

}
