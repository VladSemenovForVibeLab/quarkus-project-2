package ru.semenov.stepik.mapper;

import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.semenov.stepik.exception.HandledServiceException;

@Provider
public class HandledServiceExceptionMapper implements ExceptionMapper<HandledServiceException> {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    public HandledServiceExceptionMapper() {
    }

    public Response toResponse(HandledServiceException exception) {
        this.logger.error("Ошибка работы сервиса", exception);
        return Response.status(400).type("application/json").build();
    }
}
