package ru.semenov.stepik.entity;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "HB_CATEGORY")
@Data
public class Category extends PanacheEntityBase {

    @Id
    @Column(name = "ID")
    public Long id;

    @Column(name = "SYSNAME")
    public String sysName;

    @Column(name = "NAME")
    public String name;

}
