package ru.semenov.stepik.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import jakarta.persistence.*;

@Entity
@Table(name = "SQ_DRIVERMEDICALCERTCATEGORY")
public class DriverMedicalCertificateCategory extends PanacheEntityBase {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_DRIVERMEDICALCERTCATEGORY_SEQ")
    @SequenceGenerator(name = "SQ_DRIVERMEDICALCERTCATEGORY_SEQ", sequenceName = "SQ_DRIVERMEDICALCERTCATEGORY_SEQ", allocationSize = 10)
    public Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "DRIVERMEDICALCERTID")
    @JsonIgnore
    public DriverMedicalCertificate driverMedicalCertificate;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "CATEGORYID")
    public Category category;

    @Column(name = "NOTE")
    public String note;
}
