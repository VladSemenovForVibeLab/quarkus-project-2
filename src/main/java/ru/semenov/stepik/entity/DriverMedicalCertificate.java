package ru.semenov.stepik.entity;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import jakarta.persistence.*;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "SQ_DRIVERMEDICALCERT")
public class DriverMedicalCertificate extends PanacheEntityBase {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_DRIVERMEDICALCERT_SEQ")
    @SequenceGenerator(name = "SQ_DRIVERMEDICALCERT_SEQ", sequenceName = "SQ_DRIVERMEDICALCERT_SEQ", allocationSize = 10)
    public Long id;

    @Column(name = "NUMBER")
    public String number;

    @Column(name = "CREATEDATE")
    public LocalDate createDate;

    @Column(name = "SURNAME")
    public String surname;

    @Column(name = "NAME")
    public String name;

    @Column(name = "PATRONYMIC")
    public String patronymic;

    @Column(name = "BIRTHDATE")
    public LocalDate birthDate;

    @Column(name = "GENDER")
    public String gender;

    @OneToMany(mappedBy = "driverMedicalCertificate", fetch = FetchType.EAGER, cascade =
            CascadeType.ALL)
    public Set<DriverMedicalCertificateCategory> driverMedicalCertificateCategories = new HashSet<>();

    public void addDriverMedicalCertificateCategory(DriverMedicalCertificateCategory
                                                            driverMedicalCertificateCategory) {
        driverMedicalCertificateCategory.driverMedicalCertificate = this;
        this.driverMedicalCertificateCategories.add(driverMedicalCertificateCategory);
    }

    public String getFIO() {
        if (patronymic != null && !patronymic.isEmpty()) {
            return String.format("%s %s %s", surname, name, patronymic);
        } else {
            return String.format("%s %s", surname, name);
        }
    }
}
