package ru.semenov.stepik.scheduled;

import io.quarkus.scheduler.Scheduled;
import jakarta.enterprise.context.ApplicationScoped;

import java.util.concurrent.atomic.AtomicInteger;

@ApplicationScoped
public class CounterBean {

    private AtomicInteger counter = new AtomicInteger();

    @Scheduled(every = "10s")
    //@Scheduled(every = "10s", identity = "task-job")
    public void increment() {
        counter.incrementAndGet();
    }
}
