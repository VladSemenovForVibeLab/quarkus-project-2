package ru.semenov.stepTwo.entity;

import java.io.Serializable;

public class Mobile extends BaseEntity implements Serializable {
    private  String brand;
    private Integer ram;
    private Integer externalStorage;

    public Mobile(String brand, Integer ram, Integer externalStorage) {
        this.brand = brand;
        this.ram = ram;
        this.externalStorage = externalStorage;
    }

    public Mobile() {
    }

    @Override
    public String toString() {
        return "Mobile{" +
                "brand='" + brand + '\'' +
                ", ram=" + ram +
                ", externalStorage=" + externalStorage +
                '}';
    }

    public Mobile(String id, String name, String brand, Integer ram, Integer externalStorage) {
        super(id, name);
        this.brand = brand;
        this.ram = ram;
        this.externalStorage = externalStorage;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Integer getRam() {
        return ram;
    }

    public void setRam(Integer ram) {
        this.ram = ram;
    }

    public Integer getExternalStorage() {
        return externalStorage;
    }

    public void setExternalStorage(Integer externalStorage) {
        this.externalStorage = externalStorage;
    }

}
