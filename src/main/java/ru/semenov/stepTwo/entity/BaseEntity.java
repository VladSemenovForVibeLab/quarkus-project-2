package ru.semenov.stepTwo.entity;

import java.io.Serializable;
import java.util.Objects;

public class BaseEntity implements Serializable,Comparable<BaseEntity> {
    private String id;
    private String name;

    @Override
    public String toString() {
        return "BaseEntity{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BaseEntity that = (BaseEntity) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BaseEntity() {
    }

    public BaseEntity(String id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public int compareTo(BaseEntity o) {
        return this.name.compareTo(o.name);
    }
}
