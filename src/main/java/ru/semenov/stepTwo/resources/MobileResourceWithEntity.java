package ru.semenov.stepTwo.resources;

import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import ru.semenov.stepTwo.entity.Mobile;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;

@Path("/entities/mobile")
public class MobileResourceWithEntity {
    List<Mobile> mobileList = new ArrayList<>();

    /**
     * Этот метод обрабатывает GET-запросы по указанному эндпоинту.
     * Метод возвращает список всех мобильных имен из списка mobileList в формате JSON.
     * Возвращаемый результат будет упакован в объект типа Response с кодом 200 OK.
     *
     * @return
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMobileList() {
        return Response.ok(mobileList).build();
    }


    /**
     * Этот метод обрабатывает POST-запросы по указанному эндпоинту.
     * Метод принимает объект типа Mobile в формате JSON и добавляет его в список mobileList.
     * Входной параметр метода - mobile - представляет собой информацию о мобильном телефоне для добавления в список.
     * Возвращает объект типа Response с обновленным списком mobileList в формате JSON.
     *
     * @param mobile
     * @return Response
     */
    @POST
    @Path("/create")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createMobile(Mobile mobile) {
        mobileList.add(mobile);
        return Response.ok(mobile).build();
    }

    /**
     * Обновляет информацию о мобильном устройстве по его идентификатору.
     *
     * @param id             - идентификатор мобильного устройства
     * @param mobileToUpdate - обновленные данные о мобильном устройстве
     * @return - HTTP-ответ со списком всех мобильных устройств после обновления
     */
    @PUT
    @Path("/update/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateMobile(@PathParam("id") String id, Mobile mobileToUpdate) {
        mobileList = mobileList
                .stream()
                .map(mobile -> {
                    if (mobile.getId().equals(id)) {
                        return mobileToUpdate;
                    } else {
                        return mobile;
                    }
                })
                .toList();
        return Response.ok(mobileList).build();
    }


    /**
     * Метод, который удаляет мобильное устройство из списка по его идентификатору.
     *
     * @param id идентификатор мобильного устройства
     * @return ответ сервера с оставшимся списком мобильных устройств после удаления, если удаление успешно,
     * или ответ сервера с кодом ошибки, если удаление не удалось.
     */
    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteMobile(@PathParam("id") String id) {
        Optional<Mobile> first = mobileList.stream()
                .filter(mobile -> mobile.getId().equals(id))
                .findFirst();
        if (first.isPresent()) {
            mobileList.remove(first.get());
            return Response.ok(mobileList).build();
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
    }
}
