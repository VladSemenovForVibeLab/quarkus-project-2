package ru.semenov.hibernateReferences.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;
import ru.semenov.hibernateReferences.entity.Citizen;

@ApplicationScoped
public class CitizenRepository implements PanacheRepository<Citizen> {
}
