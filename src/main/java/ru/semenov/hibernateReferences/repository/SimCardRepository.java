package ru.semenov.hibernateReferences.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.transaction.Transactional;
import ru.semenov.hibernateReferences.entity.SimCard;

import java.util.Optional;

@ApplicationScoped
public class SimCardRepository implements PanacheRepository<SimCard> {
    public SimCard findByIdString(String id){
        return find("id",id).firstResult();
    }

    public Optional<SimCard> findByIdOptionalString(String id) {
        SimCard simCard = find("id",id).firstResult();
        return Optional.ofNullable(simCard);
    }

    @Transactional
    public boolean deleteByIdString(String id) {
        return delete("id",id)>0;
    }
}
