package ru.semenov.hibernateReferences.dto;

import ru.semenov.hibernateReferences.entity.Citizen;
import ru.semenov.hibernateReferences.entity.Identifier;

public record CitizenWithIdentifier(Citizen citizen, Identifier identifier) {
}
