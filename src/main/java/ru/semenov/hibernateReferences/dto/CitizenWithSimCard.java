package ru.semenov.hibernateReferences.dto;

import ru.semenov.hibernateReferences.entity.Citizen;
import ru.semenov.hibernateReferences.entity.SimCard;

public record CitizenWithSimCard(Citizen citizen, SimCard simCard) {
}
