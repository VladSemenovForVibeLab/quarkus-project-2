package ru.semenov.hibernateReferences.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UuidGenerator;

import java.io.Serializable;
import java.util.Objects;

@Entity
@Cacheable
@Table(name = Identifier.TABLE_NAME)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type",discriminatorType= DiscriminatorType.STRING)
public class Identifier implements Serializable,Comparable<Identifier>,Cloneable {
    private static final String COLUMN_ID = "id";
    public static final String TABLE_NAME = "identifier";
    private static final String COLUMN_NUMBER = "number";
    private static final String COLUMN_COMPANY="company";
    @Id
    @UuidGenerator
    @GenericGenerator(strategy = "uuid2",name = "uuid")
    @GeneratedValue(generator = "system-uuid",strategy = GenerationType.UUID)
    @Column(name = COLUMN_ID,nullable = false,unique = true)
    private String id;

    @Basic(fetch = FetchType.EAGER,optional = false)
    @Column(name = COLUMN_NUMBER,nullable = false)
    private Long number;
    @Basic(fetch = FetchType.EAGER,optional = false)
    @Column(name = COLUMN_COMPANY,nullable = false)
    private String company;

    @Version
    private Long version;

    @JsonBackReference
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "citizen_id",referencedColumnName = "id")
    private Citizen citizen;

    public Citizen getCitizen() {
        return citizen;
    }

    public void setCitizen(Citizen citizen) {
        this.citizen = citizen;
    }

    @Override
    public int compareTo(Identifier o) {
        return this.number.compareTo(o.number);
    }

    public Identifier() {
    }

    public Identifier(String id, Long number, String company, Long version) {
        this.id = id;
        this.number = number;
        this.company = company;
        this.version = version;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Identifier that = (Identifier) o;
        return Objects.equals(id, that.id) && Objects.equals(number, that.number);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, number);
    }

    @Override
    public String toString() {
        return "Identifier{" +
                "id='" + id + '\'' +
                ", number=" + number +
                ", company='" + company + '\'' +
                ", version=" + version +
                '}';
    }

    @Override
    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            throw new InternalError(e.toString());
        }
    }
}
