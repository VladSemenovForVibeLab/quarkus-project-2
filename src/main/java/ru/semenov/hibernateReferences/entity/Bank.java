package ru.semenov.hibernateReferences.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UuidGenerator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Cacheable
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type",discriminatorType = DiscriminatorType.INTEGER)
@Table(name = Bank.TABLE_NAME)
public class Bank implements Serializable,Cloneable,Comparable<Bank> {
    public static final String TABLE_NAME = "bank";

    private static final String COLUMN_NAME = "name";
    private static final String COLUMN_BRANCH="branch";
    private static final String COLUMN_IFSC_CODE="ifsc_code";
    @Id
    @UuidGenerator
    @GeneratedValue(strategy = GenerationType.UUID,generator = "system-uuid")
    @GenericGenerator(name = "uuid",strategy = "uuid2")
    private String id;
    @Basic(optional = false,fetch = FetchType.EAGER)
    @Column(name = COLUMN_NAME,unique = true,nullable = false)
    private String name;

    @Basic(optional = false,fetch = FetchType.LAZY)
    @Column(name =COLUMN_BRANCH,nullable = false)
    private String branch;

    @Basic(optional = false,fetch = FetchType.EAGER)
    @Column(name = COLUMN_IFSC_CODE,nullable = false,unique = true)
    private String ifscCode;

    @JsonBackReference
    @ManyToMany(mappedBy = "bankList")
    private List<Citizen> citizenList = new ArrayList<>();

    public List<Citizen> getCitizenList() {
        return citizenList;
    }

    public void setCitizenList(List<Citizen> citizenList) {
        this.citizenList = citizenList;
    }

    @Override
    public int compareTo(Bank o) {
        return this.name.compareTo(o.name);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bank bank = (Bank) o;
        return Objects.equals(id, bank.id) && Objects.equals(name, bank.name) && Objects.equals(ifscCode, bank.ifscCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, ifscCode);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getIfscCode() {
        return ifscCode;
    }

    public void setIfscCode(String ifscCode) {
        this.ifscCode = ifscCode;
    }

    public Bank(String id, String name, String branch, String ifscCode) {
        this.id = id;
        this.name = name;
        this.branch = branch;
        this.ifscCode = ifscCode;
    }

    public Bank() {
    }


    @Override
    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            throw new InternalError(e.toString());
        }
    }
}
