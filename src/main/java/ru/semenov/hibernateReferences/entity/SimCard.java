package ru.semenov.hibernateReferences.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UuidGenerator;

import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = SimCard.TABLE_NAME)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type",discriminatorType = DiscriminatorType.INTEGER)
public class SimCard implements Serializable,Cloneable,Comparable<SimCard> {
    public static final String TABLE_NAME = "sim_card";
    private static final String COLUMN_ID = "id";

    private static final String COLUMN_NUMBER = "number";
    private static final String COLUMN_PROVIDER = "provider";
    private static final String COLUMN_IS_ACTIVE="is_active";
    @Id
    @UuidGenerator
    @GenericGenerator(name = "uuid",strategy = "uuid2")
    @GeneratedValue(strategy = GenerationType.UUID,generator = "uuid2")
    @Column(name = COLUMN_ID,nullable = false,unique = true)
    private String id;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "citizen_id",referencedColumnName = "id")
    private Citizen citizen;

    @Basic(optional = false,fetch = FetchType.EAGER)
    @Column(name = COLUMN_NUMBER,nullable = false,unique = true)
    private Long number;

    @Basic(optional = false,fetch = FetchType.EAGER)
    @Column(name = COLUMN_PROVIDER,nullable = false)
    private String provider;

    @Basic(optional = false,fetch = FetchType.EAGER)
    @Column(name = COLUMN_IS_ACTIVE,nullable = false)
    private boolean isActive = true;
    @Version
    private Long version;



    @Override
    public String toString() {
        return "SimCard{" +
                "id='" + id + '\'' +
                ", citizen=" + citizen +
                ", number=" + number +
                ", provider='" + provider + '\'' +
                ", isActive=" + isActive +
                ", version=" + version +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SimCard simCard = (SimCard) o;
        return Objects.equals(id, simCard.id) && Objects.equals(number, simCard.number);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, number);
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Citizen getCitizen() {
        return citizen;
    }

    public void setCitizen(Citizen citizen) {
        this.citizen = citizen;
    }

    @Override
    public int compareTo(SimCard o) {
        return this.number.compareTo(o.number);
    }

    @Override
    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            throw new InternalError(e.toString());
        }
    }
}
