package ru.semenov.hibernateReferences.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UuidGenerator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Table(name = Citizen.TABLE_NAME)
@DiscriminatorColumn(name = "type",discriminatorType = DiscriminatorType.INTEGER)
public class Citizen implements Serializable,Cloneable,Comparable<Citizen> {
    public static final String TABLE_NAME = "citizen";
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_NAME = "name";
    private static final String COLUMN_GENDER="gender";
    @Id
    @Basic(optional = false)
    @UuidGenerator
    @GeneratedValue(strategy = GenerationType.UUID,generator = "system-uuid")
    @GenericGenerator(name = "uuid",strategy = "uuid2")
    @Column(name = COLUMN_ID,nullable = false,unique = true)
    private String id;
    @Basic(optional = false,fetch = FetchType.EAGER)
    @Column(name = COLUMN_NAME,nullable = true)
    private String name;
    @Basic(optional = true,fetch = FetchType.LAZY)
    @Column(name = COLUMN_GENDER)
    private String gender;
    @JsonManagedReference
    @OneToOne(mappedBy = "citizen",fetch = FetchType.LAZY)
    private Identifier identifier;

    @JsonManagedReference
    @OneToMany(mappedBy = "citizen",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<SimCard> simCards = new ArrayList<>();

    @JsonManagedReference
    @ManyToMany
    @JoinTable
            (name = "citizen_bank",
            joinColumns = @JoinColumn(name = "citizen_id"),
            inverseJoinColumns = @JoinColumn(name = "bank_id"))
    List<Bank> bankList = new ArrayList<>();

    public List<Bank> getBankList() {
        return bankList;
    }

    public void setBankList(List<Bank> bankList) {
        this.bankList = bankList;
    }

    public List<SimCard> getSimCards() {
        return simCards;
    }

    public void setSimCards(List<SimCard> simCards) {
        this.simCards = simCards;
    }

    public Identifier getIdentifier() {
        return identifier;
    }

    public void setIdentifier(Identifier identifier) {
        this.identifier = identifier;
    }

    @Override
    public int compareTo(Citizen o) {
        return this.name.compareTo(o.name);
    }

    public Citizen() {
    }

    public Citizen(String id, String name, String gender) {
        this.id = id;
        this.name = name;
        this.gender = gender;
    }


    @Override
    public String toString() {
        return "Citizen{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", gender='" + gender + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Citizen citizen = (Citizen) o;
        return Objects.equals(id, citizen.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
    @Override
    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            throw new InternalError(e.toString());
        }
    }
}
