package ru.semenov.hibernateReferences.resource;

import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.panache.common.Page;
import io.quarkus.panache.common.Sort;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import ru.semenov.hibernateReferences.dto.CitizenWithIdentifier;
import ru.semenov.hibernateReferences.dto.CitizenWithSimCard;
import ru.semenov.hibernateReferences.entity.Bank;
import ru.semenov.hibernateReferences.entity.Citizen;
import ru.semenov.hibernateReferences.entity.Identifier;
import ru.semenov.hibernateReferences.entity.SimCard;
import ru.semenov.hibernateReferences.repository.BankRepository;
import ru.semenov.hibernateReferences.repository.CitizenRepository;
import ru.semenov.hibernateReferences.repository.IdentifierRepository;
import ru.semenov.hibernateReferences.repository.SimCardRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@Path(Resource.URI)
public class Resource {
    @Inject
    private SimCardRepository simCardRepository;
    @Inject
    private IdentifierRepository identifierRepository;
    @Inject
    private CitizenRepository citizenRepository;
    @Inject
    private BankRepository bankRepository;
    public static final String URI = "/resource";

    @GET
    @Path("saveDemo")
    @Transactional
    public void saveCitizen() {
        Citizen citizen = new Citizen();
        citizen.setGender("M");
        citizen.setName("Vlad");
        citizenRepository.persist(citizen);
        Identifier identifier = new Identifier();
        identifier.setNumber(randomNumberForIdentifier());
        identifier.setCompany("SVF");
        identifier.setCitizen(citizen);
        identifierRepository.persist(identifier);
    }

    private Long randomNumberForIdentifier() {
        Random random = new Random();
        Long randomNumber = random.nextLong(Long.MAX_VALUE);
        return randomNumber;
    }

    @GET
    @Path("saveDemoWithResponse")
    @Transactional
    public Response saveCitizenAndIdentifier() {
        Citizen citizen = new Citizen();
        citizen.setGender("M");
        citizen.setName("Vlad");
        citizenRepository.persist(citizen);
        Identifier identifier = new Identifier();
        identifier.setNumber(randomNumberForIdentifier());
        identifier.setCompany("SVF");
        identifier.setCitizen(citizen);
        identifierRepository.persist(identifier);
        citizen.setIdentifier(identifier);
        return Response.ok(new CitizenWithIdentifier(citizen, identifier)).build();
    }

    @GET
    @Path("saveCitizenAndSimCard")
    @Transactional
    public Response saveCitizenAndSimCard() {
        Citizen citizen = new Citizen();
        citizen.setGender("M");
        citizen.setName("Vlad");
        citizenRepository.persist(citizen);
        SimCard simCard = new SimCard();
        simCard.setCitizen(citizen);
        simCard.setActive(true);
        simCard.setNumber(randomNumberForIdentifier());
        simCard.setProvider("MTS");
        simCardRepository.persist(simCard);
        citizen.getSimCards().add(simCard);
        return Response.ok(new CitizenWithSimCard(citizen, simCard)).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/saveBanks")
    @Transactional
    public Response saveBank() {
        String[] bankNames = {"SBER", "MTS", "VTB", "GAZPROM"};
        List<Bank> savedBank = new ArrayList<>();
        for (String bankName : bankNames) {
            Bank bank = new Bank();
            bank.setName(bankName);
            bank.setBranch("IT BANK" + bankName);
            bank.setIfscCode("IFCS223" + bankName);
            bankRepository.persist(bank);
            savedBank.add(bank);
        }
        Citizen citizen = new Citizen();
        citizen.setGender("M");
        citizen.setName("Vlad");
        for (Bank bank : savedBank) {
            citizen.getBankList().add(bank);
            bank.getCitizenList().add(citizen);
        }
        citizenRepository.persist(citizen);
        return Response.status(Response.Status.OK).build();
    }

    @GET
    @Path("/sim/all")
    @Transactional
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllSim() {
        List<SimCard> simCardList = simCardRepository.listAll();
        return Response.ok(simCardList).build();
    }

    @POST
    @Path("/persist/simCard")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Response persistSimCard(@RequestBody SimCard simCard) {
        simCardRepository.persist(simCard);
        if (simCardRepository.isPersistent(simCard)) {
            return Response.ok(simCard + "\nSim card saved successfully").build();
        } else {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @GET
    @Path("/findById/sim/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findById(@PathParam("id") String id) {
        SimCard simCard = simCardRepository.findByIdString(id);
        return Response.ok(simCard).build();
    }

    @GET
    @Path("/count/sim")
    @Produces(MediaType.APPLICATION_JSON)
    public Response countSimCard() {
        long count = simCardRepository.count();
        return Response.ok(count).build();
    }

    @GET
    @Path("/listWithParams/{provider}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response listWithParams(@PathParam("provider") String provider) {
        List<SimCard> simCards = simCardRepository.list("provider", provider);
        return Response.ok(simCards).build();
    }

    @GET
    @Path("/activeList")
    @Produces(MediaType.APPLICATION_JSON)
    public Response activeList() {
        List<SimCard> simCards = simCardRepository.list("isActive", true);
        return Response.ok(simCards).build();
    }

    @GET
    @Path("/sim/findByIdOptional/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findByIdOptional(@PathParam("id") String id) {
        Optional<SimCard> simCardOptional = simCardRepository.findByIdOptionalString(id);
        if (simCardOptional.isPresent()) {
            SimCard simCard = simCardOptional.get();
            return Response.ok(simCard).build();
        } else {
            return Response.noContent().build();
        }
    }

    @GET
    @Path("/count/sim/provider/{provider}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response count(@PathParam("provider") String provider) {
        long count = simCardRepository.count("provider", provider);
        return Response.ok(count).build();
    }

    @GET
    @Path("/delete/sim/provider/{provider}")
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    public Response delete(@PathParam("provider") String provider) {
        long delete = simCardRepository.delete("provider", provider);
        return Response.ok(delete).build();
    }

    @GET
    @Path("/deleteById/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteById(@PathParam("id") String id) {
        boolean deleteById = simCardRepository.deleteByIdString(id);
        if (deleteById) {
            return Response.ok("Sim card deleted successfully").build();
        } else {
            return Response.ok("Something went wrong").build();
        }
    }

    @GET
    @Path("/sim/update/{id}/{provider}")
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    public Response updateSim(@PathParam("id") String id,
                              @PathParam("provider") String provider) {
        int update = simCardRepository.update("provider =?1 where id =?2", provider, id);
        if (update == 1) {
            return Response.ok("Sim card updated successfully").build();
        } else {
            return Response.ok("Something went wrong").build();
        }
    }

    @GET
    @Path("/sortBy")
    @Produces(MediaType.APPLICATION_JSON)
    public Response sortBySimCard() {
        List<SimCard> simCards = simCardRepository
                .list("isActive",
                        Sort.descending("provider"),
                        false);
        return Response.ok(simCards).build();
    }

    @GET
    @Path("/getSimCard/{pageNo}/size/{pageOfSize}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSimCardPage(@PathParam("pageNo") int pageNo,
                                   @PathParam("pageOfSize") int pageOfSize) {
        PanacheQuery<SimCard> all = simCardRepository.findAll();
        List<SimCard> simCardList = all.page(Page.of(pageNo,pageOfSize)).list();
        return Response.ok(simCardList).build();
    }


}

