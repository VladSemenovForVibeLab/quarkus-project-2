package ru.semenov.customConfig.resource;

import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.eclipse.microprofile.config.ConfigProvider;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import ru.semenov.customConfig.constant.Constants;

@Path("/constants")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class Resource {


    @ConfigProperty(name = "interest_rate_property",defaultValue = "10")
    private int interestRateProperty;


    @Path("/calc_interest/{amount}")
    @GET
     public Response calcInterest(@PathParam("amount") int amount){
         return Response.ok(amount* Constants.INTEREST_RATE*1/100).build();
     }
    @Path("/calc_interest/property/{amount}")
    @GET
    public Response calcInterestWithProperty(@PathParam("amount") int amount){
        return Response.ok(amount* interestRateProperty*1/100).build();
    }
    @Path("/calc_interest/property/branch/{branch}/amount/{amount}")
    @GET
    public Response calcInterestWithProperty(@PathParam("amount") int amount,
                                             @PathParam("branch") String branch){
        Integer value = ConfigProvider
                .getConfig()
                .getOptionalValue(branch.toLowerCase() + "_interest_rate_property",
                        Integer.class)
                .orElse(50);
        return Response.ok(amount* value.intValue()*1/100).build();
    }

}
