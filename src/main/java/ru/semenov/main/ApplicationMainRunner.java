package ru.semenov.main;

import io.quarkus.runtime.Quarkus;
import io.quarkus.runtime.QuarkusApplication;
import io.quarkus.runtime.annotations.QuarkusMain;

@QuarkusMain
public class ApplicationMainRunner {
    public static void main(String[] args) {
        Quarkus.run(MainQuarkusArgs.class,args);
    }
    public static class MainQuarkusArgs implements QuarkusApplication{

        @Override
        public int run(String... args) throws Exception {
           //logic
            System.out.println(">>>>>> Application is starting! <<<<<<");
            Quarkus.waitForExit();
            return 0;
        }
    }
}
