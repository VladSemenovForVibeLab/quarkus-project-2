package ru.semenov.stepOne.resources;

import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Path("/mobile")
public class MobileResource {
    List<String> mobileList = new ArrayList<String>();

    /**
     * Этот метод обрабатывает GET-запросы по указанному эндпоинту.
     * Метод возвращает список всех мобильных имен из списка mobileList в формате текста (MediaType.TEXT_PLAIN).
     *
     * @return
     */
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public List<String> getMobileList() {
        return mobileList;
    }

    /**
     * Этот метод обрабатывает POST-запросы по указанному эндпоинту.
     * Метод принимает данные в формате текста (MediaType.TEXT_PLAIN) и добавляет новое мобильное имя в список mobileList.
     * Входной параметр метода - mobileName - представляет собой имя мобильного телефона, которое нужно добавить в список.
     *
     * @param mobileName
     */
    @POST
    @Consumes(MediaType.TEXT_PLAIN)
    public void addNewMobile(String mobileName) {
        mobileList.add(mobileName);
    }

    /**
     * Этот метод обрабатывает GET-запросы по указанному эндпоинту.
     * Метод возвращает список всех мобильных имен из списка mobileList в формате текста (MediaType.TEXT_PLAIN).
     * Возвращаемый результат будет упакован в объект типа Response с кодом 200 OK.
     *
     * @return
     */
    @GET
    @Path("response")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getMobileListResponse() {
        return Response.ok(mobileList).build();
    }

    /**
     * Этот метод обрабатывает POST-запросы по указанному эндпоинту.
     * Метод принимает данные в формате текста (MediaType.TEXT_PLAIN) и добавляет новое мобильное имя в список mobileList.
     * Входной параметр метода - mobileName - представляет собой имя мобильного телефона, которое нужно добавить в список.
     * Возвращает объект типа Response с результатом операции (true, если имя успешно добавлено, иначе false) в формате JSON.
     *
     * @param mobileName
     * @return
     */
    @POST
    @Path("addResponse")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public Response addNewMobileResponse(String mobileName) {
        return Response.ok(mobileList.add(mobileName)).build();
    }

    /**
     * Этот метод обрабатывает PUT-запросы по указанному эндпоинту.
     * Метод получает два параметра из пути и запроса: oldmobilename - старое имя мобильного телефона, которое нужно заменить, и newmobilename - новое имя, на которое нужно заменить.
     * Метод обновляет список mobileList, заменяя все вхождения oldmobilename на newmobilename.
     * Возвращает объект типа Response с обновленным списком mobileList в формате текста (MediaType.TEXT_PLAIN).
     *
     * @param oldmobilename
     * @param newmobilename
     * @return
     */
    @PUT
    @Path("/{oldmobilename}")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public Response updateMobile(@PathParam("oldmobilename") String oldmobilename,
                                 @QueryParam("newmobilename") String newmobilename) {
        mobileList = mobileList.stream().map(mobile -> {
            if (mobile.equals(oldmobilename)) {
                return newmobilename;
            } else {
                return mobile;
            }
        }).collect(Collectors.toList());
        return Response.ok(mobileList).build();
    }

    /**
     * Этот метод обрабатывает DELETE-запросы по указанному эндпоинту.
     * Метод получает параметр из пути запроса - mobileNameToDelete, представляющий собой имя мобильного телефона, которое нужно удалить из списка.
     * Метод удаляет указанное имя мобильного телефона из списка mobileList.
     * Если удаление происходит успешно, метод возвращает объект типа Response с кодом 204 No Content.
     * Если имя мобильного телефона не найдено в списке, метод возвращает объект типа Response с кодом 400 Bad Request.
     *
     * @param mobileNameToDelete
     * @return
     */
    @DELETE
    @Path("/{mobileNameToDelete}")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public Response deleteMobile(@PathParam("mobileNameToDelete") String mobileNameToDelete) {
        boolean remove = mobileList.remove(mobileNameToDelete);
        if (remove) {
            return Response.noContent().build();
        } else {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }
}
