package ru.semenov.stepTwelve;

import io.quarkus.runtime.ShutdownEvent;
import io.quarkus.runtime.StartupEvent;
import jakarta.enterprise.event.Observes;

public class CarFunction {
    public void setRace(@Observes StartupEvent startupEvent){
        System.out.println("Car is running");
    }
    public void setBreak(@Observes ShutdownEvent shutdownEvent){
        System.out.println("Applying the break");
    }
}
