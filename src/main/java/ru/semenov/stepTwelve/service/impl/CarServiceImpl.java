package ru.semenov.stepTwelve.service.impl;

import io.quarkus.runtime.Startup;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import ru.semenov.stepTwelve.service.CarService;

@Startup
public class CarServiceImpl implements CarService {
    @PostConstruct
    public void startEngine(){
        System.out.println("Starting engine...");
    }
    @PreDestroy
    public void stopEngine(){
        System.out.println("Stopping engine...");
    }
}
