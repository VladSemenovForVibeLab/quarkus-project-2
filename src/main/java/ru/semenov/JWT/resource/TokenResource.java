package ru.semenov.JWT.resource;

import jakarta.annotation.security.RolesAllowed;
import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.eclipse.microprofile.metrics.annotation.Counted;
import org.eclipse.microprofile.metrics.annotation.Metered;
import org.eclipse.microprofile.metrics.annotation.Timed;
import org.eclipse.microprofile.openapi.annotations.enums.SecuritySchemeType;
import org.eclipse.microprofile.openapi.annotations.security.SecurityScheme;
import ru.semenov.JWT.service.TokenService;

@Path(TokenResource.TOKEN_RESOURCE_URI)
@SecurityScheme(
        scheme = "bearer",
        type = SecuritySchemeType.HTTP,
        bearerFormat = "JWT"
)
public class TokenResource {
    private final TokenService tokenService;
    public static final String TOKEN_RESOURCE_URI = "/api/v1/token";

    @Inject
    public TokenResource(TokenService tokenService) {
        this.tokenService = tokenService;
    }

    @GET
//    @RolesAllowed({"all", "admin", ""})
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/token")
    @Counted(
            name = "Number of tokens received",
            description = "Quantification that can be obtained in metrics"
    )
    @Timed(
        name = "Time Taken for Token Acquisition Endpoint",
            description = "Time taken for token acquisition"
    )
    @Metered(
            name = "Token acquisition",
            description = "Token acquisition"
    )
    public Response getToken(){
        return Response.ok(tokenService.generateToken()).build();
    }
}
