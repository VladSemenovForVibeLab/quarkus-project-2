package ru.semenov.JWT.service.impl;

import io.quarkus.runtime.Startup;
import io.smallrye.jwt.build.Jwt;
import jakarta.inject.Singleton;
import ru.semenov.JWT.service.TokenService;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Singleton
public class TokenServiceImpl implements TokenService {

    Set<String> roles = new HashSet<String>(Arrays.asList("admin","user","manager"));
    @Override
    public String generateToken() {
        String sign = Jwt
                .issuer("jwt-token")
                // ДОСТУП К КАКОМУ ОБЬЕКТУ
                .subject("entity")
                // НИЖЕ НАХОДИТСЯ РОЛЬ
                .groups(roles)
                .expiresAt(System.currentTimeMillis() + 3600)
                .sign();
        return sign;
    }
}
