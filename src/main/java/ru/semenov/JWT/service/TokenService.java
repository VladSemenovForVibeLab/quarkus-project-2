package ru.semenov.JWT.service;

public interface TokenService {
    String generateToken();
}
