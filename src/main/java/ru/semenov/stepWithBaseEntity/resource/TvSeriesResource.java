package ru.semenov.stepWithBaseEntity.resource;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.core.Response;
import ru.semenov.stepWithBaseEntity.entity.TvSeries;

@Path("/tvSeries")
public class TvSeriesResource {

    @GET
    @Path("/{id}")
    public Response getTvSeries(@PathParam("id") Integer id){
        return Response.ok(new TvSeries()).build();
    }
}
