package ru.semenov.stepWithBaseEntity.entity;

import java.io.Serializable;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.Objects;

public class TvSeries extends BaseEntity implements Serializable, Comparable<TvSeries> {
    private URL url;
    private String name;
    private String type;
    private String language;

    private TvSeries(Builder builder) {
        setId(builder.id);
        setDateCreated(builder.dateCreated);
        setDateUpdated(builder.dateUpdated);
        setUrl(builder.url);
        setName(builder.name);
        setType(builder.type);
        setLanguage(builder.language);
    }


    @Override
    public String toString() {
        return "TvSeries{" +
                "url=" + url +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", language='" + language + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TvSeries tvSeries = (TvSeries) o;
        return Objects.equals(url, tvSeries.url);
    }

    @Override
    public int hashCode() {
        return Objects.hash(url);
    }

    public URL getUrl() {
        return url;
    }

    public void setUrl(URL url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public TvSeries() {
    }

    public TvSeries(URL url, String name, String type, String language) {
        this.url = url;
        this.name = name;
        this.type = type;
        this.language = language;
    }

    public TvSeries(Integer id, LocalDateTime dateCreated, LocalDateTime dateUpdated, URL url, String name, String type, String language) {
        super(id, dateCreated, dateUpdated);
        this.url = url;
        this.name = name;
        this.type = type;
        this.language = language;
    }

    @Override
    public int compareTo(TvSeries o) {
        return this.name.compareTo(o.name);
    }

    public static final class Builder {
        private Integer id;
        private LocalDateTime dateCreated;
        private LocalDateTime dateUpdated;
        private URL url;
        private String name;
        private String type;
        private String language;

        public Builder() {
        }

        public Builder id(Integer val) {
            id = val;
            return this;
        }

        public Builder dateCreated(LocalDateTime val) {
            dateCreated = val;
            return this;
        }

        public Builder dateUpdated(LocalDateTime val) {
            dateUpdated = val;
            return this;
        }

        public Builder url(URL val) {
            url = val;
            return this;
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder type(String val) {
            type = val;
            return this;
        }

        public Builder language(String val) {
            language = val;
            return this;
        }

        public TvSeries build() {
            return new TvSeries(this);
        }
    }
}
