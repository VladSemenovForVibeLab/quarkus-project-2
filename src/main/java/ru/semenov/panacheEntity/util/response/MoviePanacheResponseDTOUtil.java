package ru.semenov.panacheEntity.util.response;

import jakarta.enterprise.context.ApplicationScoped;
import ru.semenov.panacheEntity.dto.response.MoviePanacheResponseDTO;
import ru.semenov.panacheEntity.entity.MoviePanache;
import ru.semenov.restEasy.util.EntityDTOUtil;

import java.util.List;
@ApplicationScoped
public class MoviePanacheResponseDTOUtil implements EntityDTOUtil<MoviePanache, MoviePanacheResponseDTO> {
    @Override
    public MoviePanache toEntity(MoviePanacheResponseDTO dto) {
        return MoviePanache
                .builder()
                .director(dto.getDirector())
                .title(dto.getTitle())
                .country(dto.getCountry())
                .description(dto.getDescription())
                .build();
    }

    @Override
    public MoviePanacheResponseDTO toDTO(MoviePanache entity) {
        return new MoviePanacheResponseDTO(entity);
    }

    @Override
    public List<MoviePanache> toEntityList(List<MoviePanacheResponseDTO> dtoList) {
        return dtoList.stream().map(this::toEntity).toList();
    }

    @Override
    public List<MoviePanacheResponseDTO> toDTOList(List<MoviePanache> entityList) {
        return entityList.stream().map(this::toDTO).toList();
    }
}
