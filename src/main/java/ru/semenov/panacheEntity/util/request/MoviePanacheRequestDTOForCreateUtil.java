package ru.semenov.panacheEntity.util.request;

import jakarta.enterprise.context.ApplicationScoped;
import ru.semenov.panacheEntity.dto.request.MoviePanacheRequestDTOForCreate;
import ru.semenov.panacheEntity.entity.MoviePanache;
import ru.semenov.restEasy.util.EntityDTOUtil;

import java.util.List;

@ApplicationScoped
public class MoviePanacheRequestDTOForCreateUtil implements EntityDTOUtil<MoviePanache, MoviePanacheRequestDTOForCreate> {
    @Override
    public MoviePanache toEntity(MoviePanacheRequestDTOForCreate dto) {
        return MoviePanache.builder()
                .description(dto.getDescription())
                .country(dto.getCountry())
                .title(dto.getTitle())
                .director(dto.getDirector())
                .build();
    }

    @Override
    public MoviePanacheRequestDTOForCreate toDTO(MoviePanache entity) {
        return new MoviePanacheRequestDTOForCreate(entity);
    }

    @Override
    public List<MoviePanache> toEntityList(List<MoviePanacheRequestDTOForCreate> dtoList) {
        return dtoList.stream().map(this::toEntity).toList();
    }

    @Override
    public List<MoviePanacheRequestDTOForCreate> toDTOList(List<MoviePanache> entityList) {
        return entityList.stream().map(this::toDTO).toList();
    }
}
