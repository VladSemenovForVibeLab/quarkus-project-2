package ru.semenov.panacheEntity.resource;

import jakarta.transaction.Transactional;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import ru.semenov.panacheEntity.entity.Laptop;

import java.net.URI;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Path("/laptops")
public class LaptopResource {
    @GET
    @Path("/all")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllLaptops(){
        List<Laptop> laptopList = Laptop.listAll();
        return Response.ok(laptopList).build();
    }
    @POST
    @Path("/add")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Response saveLaptop(Laptop laptop){
        Laptop.persist(laptop);
        if(laptop.isPersistent()){
            return Response.created(URI.create("/laptops/"+laptop.id)).build();
        }else{
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getLaptop(@PathParam("id") Long id){
        Laptop laptopById = Laptop.findById(id);
        return Response.ok(laptopById).build();
    }

    @PUT
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Response putLaptop(@PathParam("id") Long id,
                              Laptop updateLaptop){
        Optional<Laptop> byIdOptional = Laptop.findByIdOptional(id);
        if(byIdOptional.isPresent()){
            Laptop dbLaptop = byIdOptional.get();
            if(Objects.nonNull(updateLaptop.getName())){
                dbLaptop.setName(updateLaptop.getName());
            }
            if(Objects.nonNull(updateLaptop.getBrand())){
                dbLaptop.setBrand(updateLaptop.getBrand());
            }
            if(Objects.nonNull(updateLaptop.getRam())){
                dbLaptop.setRam(updateLaptop.getRam());
            }
            if(Objects.nonNull(updateLaptop.getExternalStorage())){
                dbLaptop.setExternalStorage(updateLaptop.getExternalStorage());
            }
            dbLaptop.persist();
            if(dbLaptop.isPersistent()){
                return Response.created(URI.create("/laptops/"+id)).build();
            }else {
                return Response.status(Response.Status.BAD_REQUEST).build();
            }
        }else {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    public Response deleteLaptop(@PathParam("id") Long id){
        boolean isDeleted = Laptop.deleteById(id);
        if(isDeleted){
            return Response.noContent().build();
        }else {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }
}
