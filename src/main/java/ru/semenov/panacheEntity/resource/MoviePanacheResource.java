package ru.semenov.panacheEntity.resource;

import jakarta.inject.Inject;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.eclipse.microprofile.metrics.annotation.Counted;
import org.eclipse.microprofile.metrics.annotation.Metered;
import org.eclipse.microprofile.metrics.annotation.Timed;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import ru.semenov.panacheEntity.dto.request.MoviePanacheRequestDTOForCreate;
import ru.semenov.panacheEntity.dto.response.MoviePanacheResponseDTO;
import ru.semenov.panacheEntity.entity.MoviePanache;
import ru.semenov.panacheEntity.service.MoviePanacheService;
import ru.semenov.panacheEntity.util.response.MoviePanacheResponseDTOUtil;
import ru.semenov.restEasy.dto.request.MovieRequestDTOForCreate;
import ru.semenov.restEasy.dto.response.MovieResponseDTO;
import ru.semenov.restEasy.entity.Movie;
import ru.semenov.restEasy.exception.MovieNotCreateException;

import java.util.List;

@Path(MoviePanacheResource.MOVIE_PANACHE_RESOURCE_URL)
@Tag(name = "Контрллер с фильмами с использованиме extends entity by Panache")
public class MoviePanacheResource {
    public static final String MOVIE_PANACHE_RESOURCE_URL ="/api/v1/movies-panache";
    private final MoviePanacheService moviePanacheService;
    private final MoviePanacheResponseDTOUtil moviePanacheResponseDTOUtil;
    @Inject
    public MoviePanacheResource(MoviePanacheService moviePanacheService, MoviePanacheResponseDTOUtil moviePanacheResponseDTOUtil) {
        this.moviePanacheService = moviePanacheService;
        this.moviePanacheResponseDTOUtil = moviePanacheResponseDTOUtil;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{pageNo}/{pageOfSize}")
    @Counted(
            name = "getAllPMoviePanache_counter",
            description = "Счетчик вызовов метода getAllPMoviePanache"
    )
    @Timed(
            name = "getAllPMoviePanache_timer",
            description = "Таймер замеров времени выполнения метода getAllPMoviePanache"
    )
    @Metered(
            name = "getAllPMoviePanache_meter",
            description = "Счетчик метрик вызовов метода getAllPMoviePanache"
    )
    @Operation(
            operationId = "getAllPMoviePanache",
            summary = "Все фильмы",
            description = "Получение всех филмов с пагинацией"
    )
    @APIResponse(
            responseCode = "200",
            description = "Фильмы получены",
            content = @Content(mediaType = MediaType.APPLICATION_JSON)
    )
    public Response getAllPMovie(
            @Parameter(
                    description = "pageNo - номер страницы которая будет отображена",
                    required = true,
                    example = "0"
            )
            @PathParam("pageNo") int pageNo,
            @Parameter(
                    description = "pageOfSize - количество отображеемой информации на странице",
                    required = true,
                    example = "1"
            )
            @PathParam("pageOfSize") int pageOfSize) {
        List<MoviePanache> all = moviePanacheService.getAll(pageNo, pageOfSize);
        List<MoviePanacheResponseDTO> responseDTOS = moviePanacheResponseDTOUtil.toDTOList(all);
        return Response.ok(responseDTOS).build();
    }
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Counted(
            name = "getMoviePanacheById_counter",
            description = "Счетчик вызовов метода getMoviePanacheById"
    )
    @Timed(
            name = "getMoviePanacheById_timer",
            description = "Таймер замеров времени выполнения метода getMoviePanacheById"
    )
    @Metered(
            name = "getMoviePanacheById_meter",
            description = "Счетчик метрик вызовов метода getMoviePanacheById"
    )
    @Path("/{id}")
    @Operation(
            operationId = " getMoviePanacheById",
            summary = "Нахождение  фильмов Panache",
            description = "Отправка id и получение DTO в ответ -> отправленное находится в БД по id"
    )
    @APIResponse(
            responseCode = "200",
            description = "Фильм найден",
            content = @Content(mediaType = MediaType.APPLICATION_JSON)
    )
    public Response getMoviePanacheById(
            @Parameter(
                    description = "id - id MoviePanache",
                    required = true,
                    example = "1"
            )
            @PathParam("id") Long id) {
        MoviePanache moviePanacheById = moviePanacheService.getMoviePanacheById(id);
        if (moviePanacheById == null) {
            return Response.status(HttpServletResponse.SC_NOT_FOUND).build();
        }
        MoviePanacheResponseDTO dto = moviePanacheResponseDTOUtil.toDTO(moviePanacheById);
        return Response.ok(dto).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Counted(
            name = "getMoviePanacheByCountry_counter",
            description = "Счетчик вызовов метода getMoviePanacheByCountry"
    )
    @Timed(
            name = "getMoviePanacheByCountry_timer",
            description = "Таймер замеров времени выполнения метода getMoviePanacheByCountry"
    )
    @Metered(
            name = "getMoviePanacheByCountry_meter",
            description = "Счетчик метрик вызовов метода getMoviePanacheByCountry"
    )
    @Path("/country/{country}")
    @Operation(
            operationId = " getMoviePanacheByCountry",
            summary = "Нахождение  фильмов Panache по стране",
            description = "Отправка country и получение DTO в ответ -> отправленное находится в БД по country"
    )
    @APIResponse(
            responseCode = "200",
            description = "Фильм найден по country",
            content = @Content(mediaType = MediaType.APPLICATION_JSON)
    )
    public Response getMoviePanacheByCountry(
            @Parameter(
                    description = "Country - страна создания фильма",
                    required = true,
                    example = "США"
            )
            @PathParam("country") String country) {
        List<MoviePanache> moviePanachesByCountry = moviePanacheService.getMoviePanachesByCountry(country);
        if (moviePanachesByCountry == null) {
            return Response.status(HttpServletResponse.SC_NOT_FOUND).build();
        }
        List<MoviePanacheResponseDTO> dtos = moviePanacheResponseDTOUtil.toDTOList(moviePanachesByCountry);
        return Response.ok(dtos).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Counted(
            name = "getMoviePanacheByTitle_counter",
            description = "Счетчик вызовов метода getMoviePanacheByTitle"
    )
    @Timed(
            name = "getMoviePanacheByTitle_timer",
            description = "Таймер замеров времени выполнения метода getMoviePanacheByTitle"
    )
    @Metered(
            name = "getMoviePanacheByTitle_meter",
            description = "Счетчик метрик вызовов метода getMoviePanacheByTitle"
    )
    @Path("/title/{title}")
    @Operation(
            operationId = " getMoviePanacheByTitle",
            summary = "Нахождение  фильмов Panache по названию",
            description = "Отправка title и получение DTO в ответ -> отправленное находится в БД по title"
    )
    @APIResponse(
            responseCode = "200",
            description = "Фильм найден по title",
            content = @Content(mediaType = MediaType.APPLICATION_JSON)
    )
    public Response getMoviePanacheByTitle(
            @Parameter(
                    description = "title - название нашего фильма",
                    required = true,
                    example = "Сорвиголова Кик Бутовски"
            )
            @PathParam("title") String title) {
        MoviePanache moviePanacheByTitle = moviePanacheService.getMoviePanacheByTitle(title);
        if (moviePanacheByTitle == null) {
            return Response.status(HttpServletResponse.SC_NOT_FOUND).build();
        }
        MoviePanacheResponseDTO dto = moviePanacheResponseDTOUtil.toDTO(moviePanacheByTitle);
        return Response.ok(dto).build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Counted(
            name = "addNewMoviePanache_counter",
            description = "Счетчик вызовов метода addNewMoviePanache"
    )
    @Timed(
            name = "addNewMoviePanache_timer",
            description = "Таймер замеров времени выполнения метода addNewMoviePanache"
    )
    @Metered(
            name = "addNewMoviePanache_meter",
            description = "Счетчик метрик вызовов метода addNewMoviePanache"
    )
    @Operation(
            operationId = "addNewMoviePanache",
            summary = "Добавление новых фильмов Panache",
            description = "Отправка DTO и получение DTO в ответ -> отправленное сохраняется в БД"
    )
    @APIResponse(
            responseCode = "200",
            description = "Фильм сохранен",
            content = @Content(mediaType = MediaType.APPLICATION_JSON)
    )
    public Response addNewMovie(@RequestBody(
            description = "DTO для создания нового фильма",
            required = true,
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = MoviePanacheRequestDTOForCreate.class))) MoviePanacheRequestDTOForCreate moviePanacheRequestDTOForCreate) throws MovieNotCreateException {
        MoviePanache moviePanache = moviePanacheService.createNewMoviePanache(moviePanacheRequestDTOForCreate);
        MoviePanacheResponseDTO dto = moviePanacheResponseDTOUtil.toDTO(moviePanache);
        return Response.ok(dto).build();
    }

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Counted(
            name = "deleteMoviePanache_counter",
            description = "Счетчик вызовов метода deleteMoviePanache"
    )
    @Timed(
            name = "deleteMoviePanache_timer",
            description = "Таймер замеров времени выполнения метода deleteMoviePanache"
    )
    @Metered(
            name = "deleteMoviePanache_meter",
            description = "Счетчик метрик вызовов метода deleteMoviePanache"
    )
    @Operation(
            operationId = "deleteMoviePanache",
            summary = "Удаление старых фильмов",
            description = "Отправка id и получение boolean в ответ -> отправленное удаляется в БД по id"
    )
    @APIResponse(
            responseCode = "200",
            description = "Фильм удален",
            content = @Content(mediaType = MediaType.APPLICATION_JSON)
    )
    public Response deleteMovie(
            @Parameter(
                    description = "id фильма, который нужно удалить",
                    example = "1",
                    required = true
            )
            @PathParam("id") Long id) {
        MoviePanache moviePanacheForDelete = moviePanacheService.getMoviePanacheById(id);
        boolean isDeleted = moviePanacheService.deleteMoviePanache(id);
        return Response.ok(isDeleted).build();
    }

}
