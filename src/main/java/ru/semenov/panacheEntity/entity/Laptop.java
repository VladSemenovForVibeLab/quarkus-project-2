package ru.semenov.panacheEntity.entity;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import jakarta.persistence.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;
@Entity
@Table(name = Laptop.TABLE_NAME)
@DiscriminatorColumn(name = "TYPE",discriminatorType = DiscriminatorType.INTEGER)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Laptop extends PanacheEntity implements Serializable,Comparable<Laptop> {

    public static final String TABLE_NAME = "Laptop";
    private static final String COLUMN_NAME = "name";
    private static final String COLUMN_BRAND="brand";
    private static final String COLUMN_RAM="ram";
    private static final String COLUMN_EXTERNAL_STORAGE="external_storage";
    private LocalDateTime dateCreated;
    private LocalDateTime dateUpdated;

    @Column(name = COLUMN_NAME, nullable = false,unique = true,length = 100)
    private String name;
    @Column(name = COLUMN_BRAND, nullable = false,length = 100)
    private String brand;
    @Column(name = COLUMN_RAM, nullable = false)
    private Integer ram;
    @Column(name = COLUMN_EXTERNAL_STORAGE,nullable = false)
    private Integer externalStorage;

    @PrePersist
    public void onCreate(){
        this.dateCreated = LocalDateTime.now();
        this.dateUpdated = LocalDateTime.now();
    }
    @PreUpdate
    public void onUpdate(){
        this.dateUpdated = LocalDateTime.now();
    }
    @Override
    public int compareTo(Laptop o) {
        return this.name.compareTo(o.name);
    }


    public Laptop() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Laptop laptop = (Laptop) o;
        return Objects.equals(name, laptop.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return "Laptop{" +
                "dateCreated=" + dateCreated +
                ", dateUpdated=" + dateUpdated +
                ", name='" + name + '\'' +
                ", brand='" + brand + '\'' +
                ", ram=" + ram +
                ", externalStorage=" + externalStorage +
                '}';
    }

    public LocalDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(LocalDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public LocalDateTime getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(LocalDateTime dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Integer getRam() {
        return ram;
    }

    public void setRam(Integer ram) {
        this.ram = ram;
    }

    public Integer getExternalStorage() {
        return externalStorage;
    }

    public void setExternalStorage(Integer externalStorage) {
        this.externalStorage = externalStorage;
    }

    public Laptop(LocalDateTime dateCreated, LocalDateTime dateUpdated, String name, String brand, Integer ram, Integer externalStorage) {
        this.dateCreated = dateCreated;
        this.dateUpdated = dateUpdated;
        this.name = name;
        this.brand = brand;
        this.ram = ram;
        this.externalStorage = externalStorage;
    }
}
