package ru.semenov.panacheEntity.entity;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.*;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.validator.constraints.Length;
import ru.semenov.panacheEntity.listener.MoviePanacheListener;

import java.io.Serializable;
@Entity(name = MoviePanache.ENTITY_NAME)
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Table(name = MoviePanache.TABLE_NAME,
        uniqueConstraints = {
                @UniqueConstraint(name = "UNIQUE_MOVIE_PANACHE_TITLE_ID",
                        columnNames = {
                                "id",
                                "title"
                        })
        },
        indexes = {
                @Index(name = "IND_MOVIE_PANACHE_ID",
                        columnList =
                                "id")
        }
)
@Getter(lazy = false,
        value = AccessLevel.PUBLIC)
@Setter(value = AccessLevel.PUBLIC)
@NoArgsConstructor
@AllArgsConstructor
@ToString(
        callSuper = true,
        includeFieldNames = true,
        doNotUseGetters = false,
        onlyExplicitlyIncluded = true)
@EqualsAndHashCode(
        cacheStrategy = EqualsAndHashCode.CacheStrategy.LAZY,
        callSuper = false,
        doNotUseGetters = false,
        onlyExplicitlyIncluded = true)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(
        name = "type",
        discriminatorType = DiscriminatorType.INTEGER)
@DiscriminatorValue(value = "1")
@NamedQueries({
        @NamedQuery(name = "MoviePanache.getAllMovies", query = "SELECT m FROM MoviePanache m"),
        @NamedQuery(name = "MoviePanache.getMovieByTitle", query = "SELECT m FROM MoviePanache m WHERE m.title = :title"),
        @NamedQuery(name = "MoviePanache.getMoviesByDirector", query = "SELECT m FROM MoviePanache m WHERE m.director = :director")
})
@NamedNativeQueries({
        @NamedNativeQuery(
                name = "MoviePanache.getMoviesByCountryNative",
                query = "SELECT * FROM movie_panache WHERE country = :country",
                resultClass = MoviePanache.class
        ),
        @NamedNativeQuery(
                name = "MoviePanache.getMoviesByDirectorNative",
                query = "SELECT * FROM movie_panache WHERE director = :director",
                resultClass = MoviePanache.class
        )
})
@NamedEntityGraph(name = "MoviePanacheGraph",
        attributeNodes = {
                @NamedAttributeNode("title"),
                @NamedAttributeNode("description"),
                @NamedAttributeNode("director"),
                @NamedAttributeNode("country")
        }
)
@Builder
@EntityListeners({MoviePanacheListener.class})
public class MoviePanache extends PanacheEntity implements Serializable,Cloneable,Comparable<MoviePanache> {
    public static final String ENTITY_NAME = "MoviePanache";
    public static final String TABLE_NAME = "movie_panache";
    private static final String COLUMN_TITLE = "title";
    private static final String COLUMN_DIRECTOR = "director";
    private static final String COLUMN_DESCRIPTION = "description";
    private static final String COLUMN_COUNTRY = "country";
    @Column(name = COLUMN_TITLE, nullable = false, unique = true,columnDefinition = "VARCHAR(255)")
    @Basic(optional = false,fetch = FetchType.EAGER)
    @ToString.Include
    @NotBlank(message = "Название не должно быть пустым!")
    @EqualsAndHashCode.Include
    @NotNull(message = "Название не должно быть null!")
    @Size(min = 3,max = 300)
    @Length(min = 3,max = 300,message = "Название должно быть в диапазоне от 3 до 300 символов")
    private String title;
    @Lob
    @Column(name = COLUMN_DESCRIPTION, nullable = false, unique = false)
    @Basic(optional = false,fetch = FetchType.EAGER)
    @ToString.Include
    @NotBlank(message = "Описание не может быть пустым!")
    @NotNull(message = "Описание не может быть null!")
    private String description;
    @Column(name = COLUMN_DIRECTOR, nullable = false, unique = false,columnDefinition = "VARCHAR(255)")
    @Basic(optional = false,fetch = FetchType.EAGER)
    @ToString.Include
    @NotBlank(message = "Директор не может быть пустым!")
    @NotNull(message = "Директор не может быть null!")
    @Size(min = 3,max = 300)
    @Length(min = 3,max = 300,message = "Имя директора должно быть в диапазоне от 3 до 300 символов")
    private String director;
    @Column(name = COLUMN_COUNTRY, nullable = false, unique = false,columnDefinition = "VARCHAR(255)")
    @Basic(optional = false,fetch = FetchType.EAGER)
    @ToString.Include
    @NotBlank(message = "Страна не может быть пустой!")
    @NotNull(message = "Страна не может быть null!")
    @Size(min = 3,max = 300)
    @Length(min = 3,max = 300,message = "Страна должен быть в диапазоне от 3 до 300 символов")
    private String country;
    @Override
    public int compareTo(MoviePanache o) {
        return this.title.compareTo(o.title);
    }
    @Override
    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            throw new InternalError(e.toString());
        }
    }
}
