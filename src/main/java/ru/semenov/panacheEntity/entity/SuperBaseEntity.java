package ru.semenov.panacheEntity.entity;

import jakarta.persistence.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UuidGenerator;

import java.time.LocalDateTime;

@MappedSuperclass
public class SuperBaseEntity{
    @Id
    @UuidGenerator
    @GeneratedValue(strategy = GenerationType.UUID,generator = "system-uuid")
    @GenericGenerator(name = "uuid",strategy = "uuid2")
    private String id;
    private LocalDateTime dateCreated;
    private LocalDateTime dateUpdated;
    @PrePersist
    public void onCreate(){
        this.dateCreated = LocalDateTime.now();
        this.dateUpdated = LocalDateTime.now();
    }
    @PreUpdate
    public void onUpdate(){
        this.dateUpdated = LocalDateTime.now();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public LocalDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(LocalDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public LocalDateTime getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(LocalDateTime dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public SuperBaseEntity() {
    }

    public SuperBaseEntity(String id, LocalDateTime dateCreated, LocalDateTime dateUpdated) {
        this.id = id;
        this.dateCreated = dateCreated;
        this.dateUpdated = dateUpdated;
    }
}

