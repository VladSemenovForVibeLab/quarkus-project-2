package ru.semenov.panacheEntity.service;

import ru.semenov.panacheEntity.dto.request.MoviePanacheRequestDTOForCreate;
import ru.semenov.panacheEntity.entity.MoviePanache;
import ru.semenov.restEasy.exception.MovieNotCreateException;

import java.util.List;

public interface MoviePanacheService {
    List<MoviePanache> getAll(int pageNo, int pageOfSize);

    MoviePanache getMoviePanacheById(Long id);

    List<MoviePanache> getMoviePanachesByCountry(String country);

    MoviePanache getMoviePanacheByTitle(String title);

    MoviePanache createNewMoviePanache(MoviePanacheRequestDTOForCreate moviePanacheRequestDTOForCreate) throws MovieNotCreateException;

    boolean deleteMoviePanache(Long moviePanacheForDeleteId);
}
