package ru.semenov.panacheEntity.service.impl;

import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.panache.common.Page;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.transaction.Transactional;
import ru.semenov.panacheEntity.dto.request.MoviePanacheRequestDTOForCreate;
import ru.semenov.panacheEntity.entity.MoviePanache;
import ru.semenov.panacheEntity.service.MoviePanacheService;
import ru.semenov.panacheEntity.util.request.MoviePanacheRequestDTOForCreateUtil;
import ru.semenov.restEasy.exception.MovieNotCreateException;

import java.util.List;

@Singleton
public class MoviePanacheServiceImpl implements MoviePanacheService {
    private final MoviePanacheRequestDTOForCreateUtil moviePanacheRequestDTOForCreateUtil;

    @Inject
    public MoviePanacheServiceImpl(MoviePanacheRequestDTOForCreateUtil moviePanacheRequestDTOForCreateUtil) {
        this.moviePanacheRequestDTOForCreateUtil = moviePanacheRequestDTOForCreateUtil;
    }

    @Override
    public List<MoviePanache> getAll(int pageNo, int pageOfSize) {
        PanacheQuery<MoviePanache> all = MoviePanache.findAll();
        List<MoviePanache> movieList = all.page(Page.of(pageNo, pageOfSize)).list();
        return movieList;
    }

    @Override
    public MoviePanache getMoviePanacheById(Long id) {
        MoviePanache moviePanache = MoviePanache.findById(id);
        return moviePanache;
    }

    @Override
    public List<MoviePanache> getMoviePanachesByCountry(String country) {
        List<MoviePanache> moviePanaches = MoviePanache.list("SELECT m FROM MoviePanache m WHERE m.country = ?1 ORDER BY id DESC",
                country);
        return moviePanaches;
    }

    @Override
    public MoviePanache getMoviePanacheByTitle(String title) {
        return MoviePanache
                .find("title", title)
                .firstResult();
    }

    @Override
    @Transactional
    public MoviePanache createNewMoviePanache(MoviePanacheRequestDTOForCreate moviePanacheRequestDTOForCreate) throws MovieNotCreateException {
        MoviePanache moviePanache = moviePanacheRequestDTOForCreateUtil.toEntity(moviePanacheRequestDTOForCreate);
        MoviePanache.persist(moviePanache);
        if(moviePanache.isPersistent()){
            return moviePanache;
        }else{
            throw new MovieNotCreateException("Movie panache not persistent", 400);
        }
    }

    @Override
    @Transactional
    public boolean deleteMoviePanache(Long moviePanacheForDeleteId) {
        boolean deleteById = MoviePanache.deleteById(moviePanacheForDeleteId);
        if(deleteById){
            return true;
        }else{
            return false;
        }
    }
}
