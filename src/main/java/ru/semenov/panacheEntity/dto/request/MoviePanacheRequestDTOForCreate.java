package ru.semenov.panacheEntity.dto.request;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import lombok.*;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import ru.semenov.panacheEntity.entity.MoviePanache;
import ru.semenov.restEasy.dto.request.MovieRequestDTOForCreate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Schema(name = MoviePanacheRequestDTOForCreate.SCHEMA_NAME,
        description = "Схема для запроса на создание фильма Panache")
public class MoviePanacheRequestDTOForCreate {
    public static final String SCHEMA_NAME = "MoviePanacheRequestDTOForCreate";
    @Schema(name = "title", description = "The title of the movie request", defaultValue = "Сорвиголова Кик Бутовски", required = true)
    @NotBlank(message = "Title is required")
    private String title;
    @NotBlank(message = "description is required")
    @Schema(name = "description", description = "The description of the movie response", defaultValue = "Сорвиголова Кик Бутовски (англ. Kick Buttowski: Suburban Daredevil) — американский мультипликационный сериал канала Disney. Пилотная серия вышла 13 февраля 2010 года, последняя — 2 декабря 2012 года. Сериал продолжался 2 сезона, за это время вышло 52 выпуска.")
    @NotEmpty(message = "Описание фильма не может быть пустым")
    private String description;
    @NotBlank(message = "Director is required")
    @Schema(name = "director", description = "The director of the movie response", defaultValue = "Сандро Корсаро")
    @NotEmpty(message = "Режиссер фильма не может быть пустым")
    private String director;
    @NotBlank(message = "Country is required")
    @Schema(name = "country", description = "The director of the movie response", defaultValue = "США")
    @NotEmpty(message = "Страна фильма не может быть пустой")
    private String country;
    public MoviePanacheRequestDTOForCreate(MoviePanache moviePanache){
        this.title= moviePanache.getTitle();
        this.country= moviePanache.getCountry();
        this.director= moviePanache.getDirector();
        this.description= moviePanache.getDescription();
    }
}
