package ru.semenov.panacheEntity.dto.response;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import lombok.*;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import ru.semenov.panacheEntity.entity.MoviePanache;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class MoviePanacheResponseDTO {
    @Schema(name = "title", description = "The title of the movie response", defaultValue = "Сорвиголова Кик Бутовски")
    @NotBlank(message = "Title is required")
    @NotEmpty(message = "Название фильма не может быть пустым")
    private String title;
    @NotBlank(message = "description is required")
    @Schema(name = "description", description = "The description of the movie response", defaultValue = "Сорвиголова Кик Бутовски (англ. Kick Buttowski: Suburban Daredevil) — американский мультипликационный сериал канала Disney. Пилотная серия вышла 13 февраля 2010 года, последняя — 2 декабря 2012 года. Сериал продолжался 2 сезона, за это время вышло 52 выпуска.")
    @NotEmpty(message = "Описание фильма не может быть пустым")
    private String description;
    @NotBlank(message = "Director is required")
    @Schema(name = "director", description = "The director of the movie response", defaultValue = "Сандро Корсаро")
    @NotEmpty(message = "Режиссер фильма не может быть пустым")
    private String director;
    @NotBlank(message = "Country is required")
    @Schema(name = "country", description = "The director of the movie response", defaultValue = "США")
    @NotEmpty(message = "Страна фильма не может быть пустой")
    private String country;
    public MoviePanacheResponseDTO(MoviePanache moviePanache){
        this.title=moviePanache.getTitle();
        this.director=moviePanache.getDirector();
        this.country= moviePanache.getCountry();
        this.description=moviePanache.getDescription();
    }
}
