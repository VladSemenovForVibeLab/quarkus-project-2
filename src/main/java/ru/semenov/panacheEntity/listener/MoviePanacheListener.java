package ru.semenov.panacheEntity.listener;

import jakarta.persistence.*;
import ru.semenov.panacheEntity.entity.MoviePanache;

public class MoviePanacheListener {


    @PrePersist
    public void prePersist(Object object) {
        if (object instanceof MoviePanache) {
            MoviePanache moviePanache = (MoviePanache) object;
            System.out.println("Фильм будет успешно сохранен в базе данных:");
            System.out.println("Название: " + moviePanache.getTitle());
            System.out.println("Режиссер: " + moviePanache.getDirector());
            System.out.println("Страна: " + moviePanache.getCountry());
            System.out.println("Описание: " + moviePanache.getDescription());
        }
    }

    @PostPersist
    public void postPersist(Object object) {
        if (object instanceof MoviePanache) {
            MoviePanache moviePanache = (MoviePanache) object;
            System.out.println("Фильм был успешно сохранен в базе данных:");
            System.out.println("Название: " + moviePanache.getTitle());
            System.out.println("Режиссер: " + moviePanache.getDirector());
            System.out.println("Страна: " + moviePanache.getCountry());
            System.out.println("Описание: " + moviePanache.getDescription());
        }
    }

    @PreUpdate
    public void preUpdate(Object object) {
        if (object instanceof MoviePanache) {
            MoviePanache moviePanache = (MoviePanache) object;
            System.out.println("Фильм будет успешно обновлен в базе данных:");
            System.out.println("Название: " + moviePanache.getTitle());
            System.out.println("Режиссер: " + moviePanache.getDirector());
            System.out.println("Страна: " + moviePanache.getCountry());
            System.out.println("Описание: " + moviePanache.getDescription());
        }
    }

    @PostUpdate
    public void postUpdate(Object object) {
        if (object instanceof MoviePanache) {
            MoviePanache moviePanache = (MoviePanache) object;
            System.out.println("Фильм был успешно изменен в базе данных:");
            System.out.println("Название: " + moviePanache.getTitle());
            System.out.println("Режиссер: " + moviePanache.getDirector());
            System.out.println("Страна: " + moviePanache.getCountry());
            System.out.println("Описание: " + moviePanache.getDescription());
        }
    }

    @PreRemove
    public void preRemove(Object object) {
        if (object instanceof MoviePanache) {
            MoviePanache moviePanache = (MoviePanache) object;
            System.out.println("Фильм будет успешно удален в базе данных:");
            System.out.println("Название: " + moviePanache.getTitle());
            System.out.println("Режиссер: " + moviePanache.getDirector());
            System.out.println("Страна: " + moviePanache.getCountry());
            System.out.println("Описание: " + moviePanache.getDescription());
        }
    }

    @PostRemove
    public void postRemove(Object object) {
        if (object instanceof MoviePanache) {
            MoviePanache moviePanache = (MoviePanache) object;
            System.out.println("Фильм был успешно удален в базе данных:");
            System.out.println("Название: " + moviePanache.getTitle());
            System.out.println("Режиссер: " + moviePanache.getDirector());
            System.out.println("Страна: " + moviePanache.getCountry());
            System.out.println("Описание: " + moviePanache.getDescription());
        }
    }

    @PostLoad
    public void postLoad(Object object) {
        if (object instanceof MoviePanache) {
            MoviePanache moviePanache = (MoviePanache) object;
            System.out.println("Фильм был успешно загружен из базы данных:");
            System.out.println("Название: " + moviePanache.getTitle());
            System.out.println("Режиссер: " + moviePanache.getDirector());
            System.out.println("Страна: " + moviePanache.getCountry());
            System.out.println("Описание: " + moviePanache.getDescription());
        }
    }
}
