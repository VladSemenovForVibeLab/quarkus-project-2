package ru.semenov.microserviceHibernate.listener;

import jakarta.persistence.*;

public class ProductListener {
    @PrePersist
    public void prePersist(Object object) {
        System.out.println("Executing prePersist method");
    }

    @PostPersist
    public void postPersist(Object object) {
        System.out.println("Executing postPersist method");
    }

    @PreUpdate
    public void preUpdate(Object object) {
        System.out.println("Executing preUpdate method");
    }

    @PostUpdate
    public void postUpdate(Object object) {
        System.out.println("Executing postUpdate method");
    }

    @PreRemove
    public void preRemove(Object object) {
        System.out.println("Executing preRemove method");
    }

    @PostRemove
    public void postRemove(Object object) {
        System.out.println("Executing postRemove method");
    }

    @PostLoad
    public void postLoad(Object object) {
        System.out.println("Executing postLoad method");
    }
}
