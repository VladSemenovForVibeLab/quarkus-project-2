package ru.semenov.microserviceHibernate.service;

import io.quarkus.hibernate.orm.panache.PanacheQuery;
import ru.semenov.microserviceHibernate.dto.ProductDTOForCreateEntity;
import ru.semenov.microserviceHibernate.entity.Product;

import java.util.List;

public interface ProductService {
    List<Product> getAll(int pageNo, int pageOfSize);

    Product createNewProduct(ProductDTOForCreateEntity productDTOForCreateEntity);
}
