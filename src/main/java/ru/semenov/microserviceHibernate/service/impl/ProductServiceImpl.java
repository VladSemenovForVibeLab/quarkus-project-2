package ru.semenov.microserviceHibernate.service.impl;

import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.panache.common.Page;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.transaction.Transactional;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validator;
import jakarta.ws.rs.core.Response;
import ru.semenov.microserviceHibernate.dto.ProductDTOForCreateEntity;
import ru.semenov.microserviceHibernate.entity.Product;
import ru.semenov.microserviceHibernate.exception.ProductNotFoundException;
import ru.semenov.microserviceHibernate.mapper.ProductMapper;
import ru.semenov.microserviceHibernate.repository.ProductRepository;
import ru.semenov.microserviceHibernate.service.ProductService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Singleton
public class ProductServiceImpl implements ProductService {
    private final ProductRepository productRepository;
    private final Validator validator;
    private final ProductMapper productMapper;

    @Inject
    public ProductServiceImpl(ProductRepository productRepository, Validator validator, ProductMapper productMapper) {
        this.productRepository = productRepository;
        this.validator = validator;
        this.productMapper = productMapper;
    }

    @Override
    public List<Product> getAll(int pageNo, int pageOfSize) {
        PanacheQuery<Product> all = productRepository.findAll();
        List<Product> productList = all.page(Page.of(pageNo,pageOfSize)).list();
        return productList;
    }

    @Override
    @Transactional
    public Product createNewProduct(ProductDTOForCreateEntity productDTOForCreateEntity) {
        if(isValidateEntity(productDTOForCreateEntity)){
            Product dao = productMapper.toDAO(productDTOForCreateEntity);
            productRepository.persist(dao);
            return dao;
        }
        throw new ProductNotFoundException("Product not found!",Integer.valueOf(404));
    }

    public boolean isValidateEntity(ProductDTOForCreateEntity entity) {
        Set<ConstraintViolation<ProductDTOForCreateEntity>> validate = validator.validate(entity);
        if (validate.isEmpty()) {
            return true;
        }
        return false;
    }
}
