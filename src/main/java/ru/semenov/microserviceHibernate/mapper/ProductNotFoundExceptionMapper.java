package ru.semenov.microserviceHibernate.mapper;

import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;
import ru.semenov.exceptionHandler.dto.ErrorMessage;
import ru.semenov.microserviceHibernate.exception.ProductNotFoundException;

@Provider
public class ProductNotFoundExceptionMapper implements ExceptionMapper<ProductNotFoundException> {

    @Override
    public Response toResponse(ProductNotFoundException e) {
        ErrorMessage errorMessage = new ErrorMessage();
        errorMessage.setStatus(e.getStatus());
        errorMessage.setMessage(e.getMessage());
        return Response
                .status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity(errorMessage)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }
}
