package ru.semenov.microserviceHibernate.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.semenov.microserviceHibernate.dto.ProductDTOForCreateEntity;
import ru.semenov.microserviceHibernate.entity.Product;

@Mapper(
        componentModel = "cdi"
)
public interface ProductMapper {
    @Mapping(target = "name",expression = "java(productDTO.getName())")
    Product toDAO(ProductDTOForCreateEntity productDTO);
    @Mapping(target = "name",expression = "java(product.getName())")
    ProductDTOForCreateEntity toDTO(Product product);
}
