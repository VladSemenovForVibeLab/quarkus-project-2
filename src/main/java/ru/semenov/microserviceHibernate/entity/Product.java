package ru.semenov.microserviceHibernate.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import lombok.*;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UuidGenerator;
import org.hibernate.validator.constraints.Length;
import ru.semenov.microserviceHibernate.listener.ProductListener;

@Entity(name = "Product")
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Table(name = Product.TABLE_NAME,
        uniqueConstraints = {
                @UniqueConstraint(name = "UNIQUE_PRODUCT_NAME_ID",
                        columnNames = {
                                "id",
                                "name"
                        })
        },
        indexes = {
                @Index(name = "IND_PRODUCT_ID",
                        columnList =
                                "id")
        }
)
@Getter(lazy = false,
        value = AccessLevel.PUBLIC)
@Setter(value = AccessLevel.PUBLIC)
@NoArgsConstructor
@AllArgsConstructor
@ToString(
        callSuper = true,
        includeFieldNames = true,
        doNotUseGetters = false,
        onlyExplicitlyIncluded = true)
@EqualsAndHashCode(
        cacheStrategy = EqualsAndHashCode.CacheStrategy.LAZY,
        callSuper = false,
        doNotUseGetters = false,
        onlyExplicitlyIncluded = true)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(
        name = "type",
        discriminatorType = DiscriminatorType.INTEGER)
@DiscriminatorValue(value = "1")
@NamedQueries({
        @NamedQuery(name = "Product.findAllProducts", query = "SELECT p FROM Product p"),
        @NamedQuery(name = "Product.findProductById", query = "SELECT p FROM Product p WHERE p.id = :id"),
        @NamedQuery(name = "Product.findProductByName", query = "SELECT p FROM Product p WHERE p.name = :name")
})
@NamedNativeQueries({
        @NamedNativeQuery(name = "Product.findAllProductsNative", query = "SELECT * FROM product"),
        @NamedNativeQuery(name = "Product.findProductByIdNative", query = "SELECT * FROM product WHERE id = :id"),
        @NamedNativeQuery(name = "Product.findProductByNameNative", query = "SELECT * FROM product WHERE name = :name")
})
@NamedEntityGraph(name = "Product.graph", attributeNodes = {
        @NamedAttributeNode("id"),
        @NamedAttributeNode("name")
})
@EntityListeners({ProductListener.class})
public class Product {
    public static final String TABLE_NAME = "product";
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_NAME = "name";
    @ToString.Include
    @EqualsAndHashCode.Include
    @Id
    @UuidGenerator
    @GeneratedValue(strategy = GenerationType.UUID, generator = "system-uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Setter(AccessLevel.NONE)
    @Column(name = COLUMN_ID, nullable = false, unique = true, insertable = false, updatable = false)
    private String id;
    @ToString.Include
    @EqualsAndHashCode.Include
    @Basic(optional = false, fetch = FetchType.EAGER)
    @Column(name = COLUMN_NAME,
            nullable = false,
            unique = true,
            insertable = true,
            updatable = true,
            columnDefinition = "VARCHAR(255)",
            length = 50)
    @NotBlank(message = "Name cannot be empty")
    @Length(min = 3,max = 100)
    private String name;
}
