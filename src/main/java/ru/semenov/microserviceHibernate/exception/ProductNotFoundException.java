package ru.semenov.microserviceHibernate.exception;

import io.quarkus.arc.All;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProductNotFoundException extends RuntimeException {
    Integer status;
    public ProductNotFoundException(String s,Integer status) {
        super(s);
        this.status = status;
    }
}
