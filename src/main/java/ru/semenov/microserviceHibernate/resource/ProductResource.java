package ru.semenov.microserviceHibernate.resource;

import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.core.Response;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import ru.semenov.microserviceHibernate.dto.ProductDTOForCreateEntity;

public interface ProductResource {
    Response getProducts(int pageNo, int pageOfSize);
    Response addNewProduct( ProductDTOForCreateEntity productDTOForCreateEntity);
}
