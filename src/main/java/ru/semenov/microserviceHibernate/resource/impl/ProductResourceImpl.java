package ru.semenov.microserviceHibernate.resource.impl;

import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.eclipse.microprofile.metrics.annotation.Counted;
import org.eclipse.microprofile.metrics.annotation.Metered;
import org.eclipse.microprofile.metrics.annotation.Timed;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import ru.semenov.microserviceHibernate.dto.ProductDTOForCreateEntity;
import ru.semenov.microserviceHibernate.entity.Product;
import ru.semenov.microserviceHibernate.mapper.ProductMapper;
import ru.semenov.microserviceHibernate.resource.ProductResource;
import ru.semenov.microserviceHibernate.service.ProductService;

import java.util.List;

@Path(ProductResourceImpl.PRODUCT_RESOURCE_URI)
public class ProductResourceImpl implements ProductResource {
    public static final String PRODUCT_RESOURCE_URI = "/api/v1/products";
    private final ProductService productService;
    private final ProductMapper productMapper;

    @Inject
    public ProductResourceImpl(ProductService productService, ProductMapper productMapper) {
        this.productService = productService;
        this.productMapper = productMapper;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{pageNo}/{pageOfSize}")
    @Counted(
            name = "getAllProduct_counter",
            description = "Счетчик вызовов метода getAllProduct"
    )
    @Timed(
            name = "getAllProduct_timer",
            description = "Таймер замеров времени выполнения метода getAllProduct"
    )
    @Metered(
            name = "getAllProduct_meter",
            description = "Счетчик метрик вызовов метода getAllProduct"
    )
    @Override
    public Response getProducts(@PathParam("pageNo") int pageNo,
                                @PathParam("pageOfSize") int pageOfSize) {
        List<Product> all = productService.getAll(pageNo, pageOfSize);
        return Response.ok(all).build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Counted(
            name = "addNewProduct_counter",
            description = "Счетчик вызовов метода addNewProduct"
    )
    @Timed(
            name = "addNewProduct_timer",
            description = "Таймер замеров времени выполнения метода addNewProduct"
    )
    @Metered(
            name = "addNewProduct_meter",
            description = "Счетчик метрик вызовов метода addNewProduct"
    )
    @Override
    public Response addNewProduct(@RequestBody ProductDTOForCreateEntity productDTOForCreateEntity){
        Product product = productService.createNewProduct(productDTOForCreateEntity);
        ProductDTOForCreateEntity dto = productMapper.toDTO(product);
        return Response.ok(dto).build();
    }

}
