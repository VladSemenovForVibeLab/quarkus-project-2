package ru.semenov.microserviceHibernate.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;
import ru.semenov.microserviceHibernate.entity.Product;

@ApplicationScoped
public class ProductRepository implements PanacheRepository<Product> {
}
