package ru.semenov.health.service.impl;

import io.quarkus.runtime.Startup;
import jakarta.inject.Inject;
import ru.semenov.health.repository.PostRepository;
import ru.semenov.health.service.PostService;

@Startup
public class PostServiceImpl implements PostService {
    private final PostRepository postRepository;

    @Inject
    public PostServiceImpl(PostRepository postRepository) {
        this.postRepository = postRepository;
    }
}
