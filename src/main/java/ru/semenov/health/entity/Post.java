package ru.semenov.health.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UuidGenerator;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = Post.TABLE_NAME)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type", discriminatorType = DiscriminatorType.INTEGER)
@NamedQueries({
        @NamedQuery(name = "Post.findAll", query = "SELECT p FROM Post p"),
        @NamedQuery(name = "Post.findByTitle", query = "SELECT p FROM Post p WHERE p.title = :title"),
        @NamedQuery(name = "Post.findByBody", query = "SELECT p FROM Post p WHERE p.body = :body"),
        @NamedQuery(name = "Post.findById", query = "SELECT p FROM Post p WHERE p.id = :id"),
        @NamedQuery(name = "Post.findByUserId", query = "SELECT p FROM Post p WHERE p.userId = :userId"),
        @NamedQuery(name = "Post.findByTitleAndBody", query = "SELECT p FROM Post p WHERE p.title = :title AND p.body = :body"),
        @NamedQuery(name = "Post.findByCreatedAt", query = "SELECT p FROM Post p WHERE p.createdAt = :createdAt")
})
@NamedNativeQueries({
        @NamedNativeQuery(name = "Post.findRecentPosts",
                query = "SELECT * FROM post WHERE created_at > NOW() - INTERVAL 7 DAY", resultClass = Post.class),
        @NamedNativeQuery(name = "Post.getPostById",
        query = "SELECT * FROM post WHERE id = :id", resultClass = Post.class),
        @NamedNativeQuery(name = "Post.findAllByNativeQuery", query = "SELECT * FROM post"),
        @NamedNativeQuery(name = "Post.findByUserIdNative",query = "SELECT * FROM post WHERE user_id = :user_id")
})
@EntityListeners(Post.class)
public class Post implements Serializable, Cloneable, Comparable<Post> {
    public static final String TABLE_NAME = "post";
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_CREATED_AT = "created_at";
    private static final String COLUMN_USER_ID = "user_id";
    private static final String COLUMN_TITLE = "title";
    private static final String COLUMN_BODY = "body";
    @Id
    @UuidGenerator
    @NotNull
    @GeneratedValue(strategy = GenerationType.UUID, generator = "system-uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = COLUMN_ID, nullable = false, unique = true)
    @Basic(optional = false, fetch = FetchType.EAGER)
    private String id;

    @NotNull
    @Column(name = COLUMN_USER_ID, nullable = true, unique = false)
    @Basic(optional = true, fetch = FetchType.EAGER)
    private String userId;

    @NotNull
    @Size(min = 3,max = 200)
    @Column(name = COLUMN_TITLE, nullable = false, unique = false)
    @Basic(optional = false, fetch = FetchType.EAGER)
    private String title;

    @NotNull
    @Size(max = 2000)
    @Column(name = COLUMN_BODY, nullable = false, unique = false)
    @Basic(optional = true, fetch = FetchType.EAGER)
    private String body;
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = COLUMN_CREATED_AT, nullable = false)
    private LocalDateTime createdAt;

    @Override
    public int compareTo(Post o) {
        return this.title.compareTo(o.title);
    }

    @Override
    public String toString() {
        return "Post{" +
                "id='" + id + '\'' +
                ", userId='" + userId + '\'' +
                ", title='" + title + '\'' +
                ", body='" + body + '\'' +
                ", createdAt=" + createdAt +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Post post = (Post) o;
        return Objects.equals(id, post.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public Post(String id, String userId, String title, String body, LocalDateTime createdAt) {
        this.id = id;
        this.userId = userId;
        this.title = title;
        this.body = body;
        this.createdAt = createdAt;
    }

    public Post() {
    }

    @Override
    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            throw new InternalError(e.toString());
        }
    }
}
