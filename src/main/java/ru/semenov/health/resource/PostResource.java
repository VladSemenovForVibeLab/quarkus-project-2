package ru.semenov.health.resource;

import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.eclipse.microprofile.metrics.MetricUnits;
import org.eclipse.microprofile.metrics.annotation.Counted;
import org.eclipse.microprofile.metrics.annotation.Gauge;
import org.eclipse.microprofile.metrics.annotation.Metered;
import org.eclipse.microprofile.metrics.annotation.Timed;
import ru.semenov.health.service.PostService;

@Path(PostResource.POST_RESOURCE_URI)
public class PostResource {

    /**
     * http://localhost:8080/q/metrics
     * http://localhost:8080/q/metrics/application
     */

    static final String POST_RESOURCE_URI = "/api/v1/posts";

    private final PostService postService;

    @Inject
    public PostResource(PostService postService) {
        this.postService = postService;
    }

    private Long highestPrimeNumberSoFar = 2L;

    @GET
    @Path("/{number}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Counted(
            name = "count of check if prime",
            description = "how many time this method/api is called"
    )
    @Timed(
            name = "time taken check if prime",
            description = "how many time this method/api take to respond"
    )
    @Metered(
            name = "Metered_CheckIfPrime",
            description = "How freq this api is called"
    )
    public Response checkIfPrime(@PathParam("number") Long number) {
        if (number < 1) {
            return Response
                    .ok("Only natural numbers can be prime numbers!")
                    .build();
        }
        if (number == 1) {
            return Response
                    .ok("1 is not prime")
                    .build();
        }
        if (number == 2) {
            return Response
                    .ok("2 is prime!")
                    .build();
        }
        if (number % 2 == 0) {
            return Response
                    .ok("is not prime, it is divisible by 2")
                    .build();
        }
        for (int i = 3; i < Math.floor(Math.sqrt(number)) + 1; i = i + 2) {
            if (number % i == 0) {
                return Response
                        .ok(number + " is not prime, is divisible by " + i + ".")
                        .build();
            }
        }
        if (number > highestPrimeNumberSoFar) {
            highestPrimeNumberSoFar = number;
        }
        return Response
                .ok(number + " is prime.")
                .build();
    }

    @Gauge(
            name = "HighestPrimeNumberRequest",
            description = "What is the highest prime number",
            unit = MetricUnits.NONE
    )
    public Long getHighestPrimeNumber() {
        return highestPrimeNumberSoFar;
    }
}
