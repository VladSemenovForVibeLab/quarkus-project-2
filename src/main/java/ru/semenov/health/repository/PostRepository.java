package ru.semenov.health.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;
import ru.semenov.health.entity.Post;
@ApplicationScoped
public class PostRepository implements PanacheRepository<Post> {
}
