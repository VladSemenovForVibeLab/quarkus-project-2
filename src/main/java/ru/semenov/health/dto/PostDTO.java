package ru.semenov.health.dto;

public record PostDTO(String userId,
                      String title,
                      String body) {
}
