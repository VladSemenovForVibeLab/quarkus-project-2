package ru.semenov;

import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;
import org.junit.jupiter.api.Test;
import ru.semenov.stepSevenTesting.entity.Student;
import ru.semenov.stepSevenTesting.repository.StudentRepository;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@QuarkusTest
public class StudentRepositoryTest {
    @Inject
    private StudentRepository studentRepository;
    @Test
    void listAll(){
        List<Student> studentList = studentRepository.listAll();
        assertTrue(studentList.isEmpty());
        assertEquals(studentList.size(),0);
    }
//    @Test
//    void findById(){
//        Student student = studentRepository.findByIdString("YOUR_ID");
//        assertNotNull(student);
//        assertEquals(student.getId(), "YOUR_ID");
//        assertEquals(student.getName(),"YOUR_NAME");
//        assertEquals(student.getBranch(),"YOUR_Branch");
//    }

    @Test
    void getStudentListByBranch(){
        List<Student> studentList = studentRepository.getStudentListByBranch("EE");
        assertTrue(studentList.isEmpty());
        assertEquals(studentList.size(),0);

    }
}
