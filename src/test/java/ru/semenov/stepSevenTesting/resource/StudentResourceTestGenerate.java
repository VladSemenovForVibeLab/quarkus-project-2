package ru.semenov.stepSevenTesting.resource;

import io.quarkus.test.InjectMock;
import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.Response;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import ru.semenov.stepSevenTesting.entity.Student;
import ru.semenov.stepSevenTesting.repository.StudentRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@QuarkusTest
class StudentResourceTestGenerate {
    @Inject
    StudentResource studentResource;
    Student student;
    @InjectMock
    StudentRepository studentRepository;

    @BeforeEach
    void setUp() {
        student=new Student();
        student.setName("Student");
        student.setBranch("CS");
    }

    @Test
    void getStudentList() {
        List<Student> students = new ArrayList<>();
        students.add(new Student(UUID.randomUUID().toString(),"Vlad","CS"));
        students.add(new Student(UUID.randomUUID().toString(),"Toly","CS"));
        students.add(new Student(UUID.randomUUID().toString(),"GENA","CS"));
        Mockito.when(studentRepository.listAll()).thenReturn(students);
        Response response = studentResource.getStudentList();
        assertNotNull(response);
        assertNotNull(response.getEntity());
        assertEquals(response.getStatus(),Response.Status.OK.getStatusCode());
        List<Student> studentList = (List<Student>) response.getEntity();
        assertEquals(studentList.size(),3);
    }

    @Test
    void getCSStudentList() {
    }

    @Test
    void addStudent() {
        Student student = new Student(UUID.randomUUID().toString(),"Vlad","SE");
        Mockito.doNothing().when(studentRepository).persist(student);
        Mockito.when(studentRepository.isPersistent(student)).thenReturn(true);
        Response response = studentResource.addStudent(student);
        assertNotNull(response);
        assertNull(response.getEntity());
        assertEquals(response.getStatus(),Response.Status.CREATED.getStatusCode());
        assertNotNull(response.getLocation());
    }

    @Test
    void getStudentById() {
        String studentId = "7934869b-aaeb-4aed-8f31-1b6016a0ca36";
        Student student1 = new Student("7934869b-aaeb-4aed-8f31-1b6016a0ca36","Vlad","ME");
        Mockito.when(studentRepository.findByIdString(ArgumentMatchers.any(String.class))).thenReturn(student1);
        Response response = studentResource.getStudentById("7934869b-aaeb-4aed-8f31-1b6016a0ca36");
        assertNotNull(response);
        assertNotNull(response.getEntity());
        assertEquals(response.getStatus(),Response.Status.OK.getStatusCode());
        Student student = (Student) response.getEntity();
        assertEquals(student.getName(),"Vlad");
        assertEquals(student.getBranch(),"ME");
    }

    @Test
    void getStudentByIdKO(){
        String studentId = "7934869b-aaeb-4aed-8f31-1b6016a0ca36";
        Mockito.when(studentRepository.findByIdString(ArgumentMatchers.any(String.class))).thenReturn(null);
        Response response = studentResource.getStudentById(studentId);
        assertNotNull(response);
        assertEquals(response.getStatus(),Response.Status.OK.getStatusCode());
    }

    @Test
    void updateStudent(){
        String studentId = "7934869b-aaeb-4aed-8f31-1b6016a0ca36";
        Student studentNew = new Student(studentId,"VLAD","ME");
        Mockito.when(studentRepository.findByIdString(ArgumentMatchers.any(String.class))).thenReturn(studentNew);
        Student updateStudent = new Student();
        updateStudent.setName("VLADOSIK");
        Response response = studentResource.updateStudent(studentId, updateStudent);
        assertNotNull(response);
        assertNotNull(response.getEntity());
        assertEquals(response.getStatus(),Response.Status.OK.getStatusCode());
        Student studentResponse = (Student) response.getEntity();
        assertEquals(studentResponse.getName(),"VLADOSIK");
        assertEquals(studentResponse.getBranch(),"ME");
    }
}