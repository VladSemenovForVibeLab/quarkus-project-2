package ru.semenov;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.RestAssured;
import jakarta.json.Json;
import jakarta.json.JsonObject;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.junit.jupiter.api.*;

import static org.hamcrest.Matchers.equalTo;

@QuarkusTest
@Tag("integration")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class StudentResourceTest {
    @Test
    @Order(1)
//    @TestSecurity(authorizationEnabled = false)
//    @TestSecurity(user = "testUser", roles = "admin)
    void addStudent() throws Exception {
        JsonObject jsonObject = Json.createObjectBuilder()
                .add("name", "VladislavOne")
                .add("branch","CS")
                .build();
        RestAssured
                .given()
                .contentType(MediaType.APPLICATION_JSON)
                .body(jsonObject.toString())
                .when()
                .post("/students/addStudent")
                .then()
                .statusCode(Response.Status.CREATED.getStatusCode());
    }
//    @Test
//    @Order(2)
//    void getStudentById() throws Exception {
//        RestAssured
//                .given()
//                .when()
//                .get("/students/8f99349d-2a1c-4d74-b261-1b9cea50ced8")
//                .then()
//                .body("name",equalTo("VladislavOne"))
//                .body("branch",equalTo("CS"));
//    }

    @Order(3)
    @Test
    void getStudentsList(){
        RestAssured
                .given()
                .get("/students/getStudentList")
                .then()
                .body("size()",equalTo(1));
    }
}
